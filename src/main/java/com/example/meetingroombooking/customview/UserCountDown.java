package com.example.meetingroombooking.customview;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.meetingroombooking.*;
import com.example.meetingroombooking.interf.CountDownForUser;
import com.example.meetingroombooking.interf.CountDownTimeInter;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Cathy on 17/4/10.
 */

public class UserCountDown extends CountDownTimer {
    private CountDownForUser countDownForUser;
    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public UserCountDown(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    @Override
    public void onTick(long millisUntilFinished) {

    }

    @Override
    public void onFinish() {
        countDownForUser.updateUser();

    }
    public void setCountDownTimeForUserInter(CountDownForUser countDownForUser) {
        this.countDownForUser = countDownForUser;
    }
}
