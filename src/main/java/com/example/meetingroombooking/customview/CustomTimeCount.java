
package com.example.meetingroombooking.customview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.example.meetingroombooking.R;
import com.example.meetingroombooking.domain.Meeting;
import com.example.meetingroombooking.domain.MyTime;
import com.example.meetingroombooking.helper.DateUtils;
import com.example.meetingroombooking.helper.LogUtils;
import com.example.meetingroombooking.interf.CountDownTimeInter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

@SuppressLint("HandlerLeak")
public class CustomTimeCount extends LinearLayout{
    private CountDownTimeInter countDownTimeInter;

    private TextSwitcher tv_hour_decade;
    private TextSwitcher tv_hour_unit;
    private TextSwitcher tv_min_decade;
    private TextSwitcher tv_min_unit;
    private TextSwitcher tv_sec_decade;
    private TextSwitcher tv_sec_unit;

    private TextView textView_minutes_vacant;
    private TextView textView_hour_vacant;
    private TextView textView_sec_vacant;

    private Context context;

    private int sec_decade;
    private int sec_unit;
    private int hour_decade;
    private int hour_unit;
    private int min_decade;
    private int min_unit;

    private Typeface typefaceGC;
    private Typeface typeFaceavermedium;
    private Timer timer;

    List<MyTime> times;
    int []time;
    int hour;
    int min;
    int sec;
    //计数的
    int count;

    private Handler handler = new Handler() {

        public void handleMessage(Message msg) {
            countDown();
        }
    };

    public void setCountDownTimeInter(CountDownTimeInter countDownTimeInter) {
        this.countDownTimeInter = countDownTimeInter;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public CustomTimeCount(final Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        typefaceGC = Typeface.createFromAsset(context.getAssets(), "Avenir Next LT W01 Ultra Light.ttf");
        typeFaceavermedium = Typeface.createFromAsset(context.getAssets(), "Avenir-Medium.otf");
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.timecount_layout, this);

        tv_sec_decade = (TextSwitcher) view.findViewById(R.id.textView_vacant_min_two);
        tv_sec_unit = (TextSwitcher) view.findViewById(R.id.textView_vacant_min);

        tv_hour_decade = (TextSwitcher) view.findViewById(R.id.textView_vacant_day_two);
        tv_hour_unit = (TextSwitcher) view.findViewById(R.id.textView_vacant_day);

        tv_min_decade = (TextSwitcher)view.findViewById(R.id.textView_vacant_hour_two);
        tv_min_unit = (TextSwitcher) view.findViewById(R.id.textView_vacant_hour);

        tv_sec_decade.setInAnimation(context, R.anim.slid_in_bottom);
        tv_sec_decade.setOutAnimation(context, R.anim.slid_in_top);
        tv_sec_decade.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView tv = new TextView(context);
                //设置文字大小
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, 256);
                //设置文字 颜色
                tv.setTextColor(Color.rgb(255, 255, 255));
                tv.setTypeface(typefaceGC);
                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
                );
//                tv.setText(strings_d[index_day] + "");
                lp.gravity = Gravity.CENTER;
                tv.setLayoutParams(lp);
                return tv;
            }
        });
//        tv_day_decade = (TextSwitcher) findViewById(R.id.textView_vacant_day_two);
        tv_sec_unit.setInAnimation(context, R.anim.slid_in_bottom);
        tv_sec_unit.setOutAnimation(context, R.anim.slid_in_top);
        tv_sec_unit.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView tv = new TextView(context);
                //设置文字大小
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, 256);
                //设置文字 颜色
                tv.setTextColor(Color.rgb(255, 255, 255));
                tv.setTypeface(typefaceGC);
                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
                );
//                tv.setText(strings_day_two[index_day_two] + "");
                lp.gravity = Gravity.CENTER;
                tv.setLayoutParams(lp);
                return tv;
            }
        });
//        tv_hour_unit = (TextSwitcher) findViewById(R.id.textView_vacant_hour);
        tv_hour_unit.setInAnimation(context, R.anim.slid_in_bottom);
        tv_hour_unit.setOutAnimation(context, R.anim.slid_in_top);
        tv_hour_unit.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView tv = new TextView(context);
                //设置文字大小
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, 256);
                //设置文字 颜色
                tv.setTextColor(Color.rgb(255, 255, 255));
                tv.setTypeface(typefaceGC);
                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
                );
                lp.gravity = Gravity.CENTER;
                tv.setLayoutParams(lp);
                return tv;
            }
        });

//        tv_hour_decade = (TextSwitcher) findViewById(R.id.textView_vacant_hour_two);
        tv_hour_decade.setInAnimation(context, R.anim.slid_in_bottom);
        tv_hour_decade.setOutAnimation(context, R.anim.slid_in_top);
        tv_hour_decade.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView tv = new TextView(context);
                //设置文字大小
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, 256);
                //设置文字 颜色
                tv.setTextColor(Color.rgb(255, 255, 255));
                tv.setTypeface(typefaceGC);
                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
                );
                lp.gravity = Gravity.CENTER;
                tv.setLayoutParams(lp);
                return tv;
            }
        });

//        tv_min_unit = (TextSwitcher) findViewById(R.id.textView_vacant_min);
        tv_min_unit.setInAnimation(context, R.anim.slid_in_bottom);
        tv_min_unit.setOutAnimation(context, R.anim.slid_in_top);
        tv_min_unit.setFactory(new ViewSwitcher.ViewFactory() {

            @Override
            public View makeView() {
                TextView tv = new TextView(context);
                //设置文字大小
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, 256);
                //设置文字 颜色
                tv.setTextColor(Color.rgb(255, 255, 255));
                tv.setTypeface(typefaceGC);
                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
                );
                lp.gravity = Gravity.CENTER;
                tv.setLayoutParams(lp);
                return tv;
            }
        });
//        tv_min_decade = (TextSwitcher) findViewById(R.id.textView_vacant_min_two);
        tv_min_decade.setInAnimation(context, R.anim.slid_in_bottom);
        tv_min_decade.setOutAnimation(context, R.anim.slid_in_top);
        tv_min_decade.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView tv = new TextView(context);
                //设置文字大小
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, 256);
                //设置文字 颜色
                tv.setTextColor(Color.rgb(255, 255, 255));
                tv.setTypeface(typefaceGC);
                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
                );
                lp.gravity = Gravity.CENTER;
                tv.setLayoutParams(lp);
                return tv;
            }
        });
        textView_minutes_vacant = (TextView) findViewById(R.id.textView_hour_vacant);
        textView_minutes_vacant.setTypeface(typeFaceavermedium);
        textView_hour_vacant = (TextView) findViewById(R.id.textView_day_vacant);
        textView_hour_vacant.setTypeface(typeFaceavermedium);
        textView_sec_vacant = (TextView) findViewById(R.id.textView_minutes_vacant);
        textView_sec_vacant.setTypeface(typeFaceavermedium);
//        textView_head_vacant = (TextView) findViewById(R.id.textView_head_vacant);
//        textView_head_vacant.setTypeface(typeFaceavermedium);

    }

    public void start() {
        if (timer == null) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.sendEmptyMessage(0);
                }
            }, 0, 1000);
        }
    }

    public void stop() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }
    List<Map<String ,Long>> mapTimes = new ArrayList<>();
    Map<String,Long> map;
    List<Boolean> isOccupieds=new ArrayList<>();
    List<List<Meeting>> meetings =new ArrayList<>();
    public void setDate(List<MyTime> times){
        mapTimes.clear();
        isOccupieds.clear();
        this.times = times;
            //在这里做数据格式转化，转换成map形式存储时间
            MyTime myTime;
            if (times.size()>0){
                for (int i =0;i<times.size();i++){
                    myTime=times.get(i);
                    try {
                        map = DateUtils.dataFormat(myTime.getEnd()+"",myTime.getStart()+"");
                        mapTimes.add(map);
                        isOccupieds.add(myTime.isOccupied());
                    } catch (ParseException e) {
                        LogUtils.e("CustomTimeCount","exception"+e.getMessage());
                    }
                }
                if (isOccupieds.size()==0){
                }else{
                    isOccupieds.add(isOccupieds.get(isOccupieds.size()-1));
                }
                setTime(mapTimes.get(0));
        }else{
                setTime(initTime());
                LogUtils.e("倒计时数组为空","到第二天0点之前，十分钟出现一次");
            }
    }

    public void setOccupied(Boolean isoccupied){
        if (isoccupied){
            countDownTimeInter.occupiedNow();
        }else{
            countDownTimeInter.vacantNow();
        }
    }

    public Map<String,Long>  initTime(){
        Map<String,Long> init =new HashMap<>();
        init.put("hour",0l);
        init.put("min",0l);
        init.put("s",0l);
        return  init;
    }

    public void setTime(Map<String ,Long> time) {
//        LogUtils.e("当前的倒计时",time.toString());
        hour = Integer.parseInt(String.valueOf(time.get("hour")));
        min=Integer.parseInt(String.valueOf(time.get("min")));
        sec = Integer.parseInt(String.valueOf(time.get("s")));
        //应该判断停止
        if (min >= 60 || sec >= 60 || min < 0 || sec < 0
                || hour < 0) {
            throw new RuntimeException("时间格式错误,请检查你的代码");
        }
        hour_decade = hour / 10;
        hour_unit = hour - hour_decade * 10;

        min_decade = min / 10;
        min_unit = min - min_decade * 10;

        sec_decade = sec / 10;
        sec_unit = sec - sec_decade * 10;

        tv_hour_decade.setText(hour_decade + "");
        tv_hour_unit.setText(hour_unit + "");
        tv_min_decade.setText(min_decade + "");
        tv_min_unit.setText(min_unit + "");
        tv_sec_decade.setText(sec_decade + "");
        tv_sec_unit.setText(sec_unit + "");
    }

    private void countDown() {
        if (isCarrysecUnit(tv_sec_unit)) {
            if (isCarrysecDecade( tv_sec_decade)) {
                if (isCarrymin4Unit(tv_min_unit)) {
                    if (isCarry4minDecade(tv_min_decade)) {
                        if (isCarryhourUnit(tv_hour_unit)) {
                            if (isCarryhourDecade(tv_hour_decade)) {
                                if (count>mapTimes.size()-1){
                                    Toast.makeText(context, "计数完成",Toast.LENGTH_SHORT).show();
//                                    LogUtils.e("当天计数完成","000000");
                                    start();
                                    stop();
                                    //停止了，数据清零。
                                    setTime(initTime());
                                } else {
                                    stop();
                                    count++;
                                    LogUtils.e("Timecount","========"+count);
                                    LogUtils.e("新一次倒计时的状态",isOccupieds.get(1)+"");
                                    setTime(mapTimes.get(0));//重新开始
                                    setOccupied(isOccupieds.get(1));
//                                  Log.e("新一次倒计时",mapTimes.get(0).toString());
                                    start();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    private boolean isCarry4minDecade(TextSwitcher tv) {
//        int time = Integer.valueOf(tv.getText().toString());
        min_decade=min_decade-1;
//        time = time - 1;
        if (min_decade < 0) {
            min_decade = 5;
            tv.setText(min_decade + "");
            return true;
        } else {
            tv.setText(min_decade + "");
            return false;
        }
    }
    private boolean isCarrymin4Unit(TextSwitcher tv) {

//        int time = Integer.valueOf(tv.getText().toString());
        min_unit = min_unit - 1;
        if (min_unit < 0) {
            min_unit = 9;
            tv.setText(min_unit + "");
            return true;
        } else {
            tv.setText(min_unit + "");
            return false;
        }
    }
    private boolean isCarrysecDecade(TextSwitcher tv) {
//        int time = Integer.valueOf(tv.getText().toString());
        sec_decade=sec_decade-1;
//        time = time - 1;
        if (sec_decade < 0) {
            sec_decade = 5;
            tv.setText(sec_decade + "");
            return true;
        } else {
            tv.setText(sec_decade + "");
            return false;
        }
    }
    private boolean isCarrysecUnit(TextSwitcher tv) {

//        int time = Integer.valueOf(tv.getText().toString());
        sec_unit = sec_unit - 1;
        if (sec_unit < 0) {
            sec_unit = 9;
            tv.setText(sec_unit + "");
            return true;
        } else {
            tv.setText(sec_unit + "");
            return false;
        }
    }
    private boolean isCarryhourDecade(TextSwitcher tv){
        hour_decade = hour_decade - 1;
        if (hour_decade < 0) {
            hour_decade = 9;
            tv.setText(hour_decade + "");
            return true;
        } else {
            tv.setText(hour_decade + "");
            return false;
        }
    }
    private boolean isCarryhourUnit(TextSwitcher tv){
        hour_unit = hour_unit - 1;
        if (hour_unit < 0) {
            hour_unit = 9;
            tv.setText(hour_unit + "");
            return true;
        } else {
            tv.setText(hour_unit + "");
            return false;
        }
    }
}
