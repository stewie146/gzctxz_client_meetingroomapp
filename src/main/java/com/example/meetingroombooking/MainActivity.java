package com.example.meetingroombooking;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meetingroombooking.adapters.MeetingAdapter;
import com.example.meetingroombooking.customview.AutoButton;
import com.example.meetingroombooking.customview.CustomTimeCount;
import com.example.meetingroombooking.customview.UserCountDown;
import com.example.meetingroombooking.domain.Meeting;
import com.example.meetingroombooking.domain.MeetingAndRooms;
import com.example.meetingroombooking.domain.Space;
import com.example.meetingroombooking.domain.SpaceBean;
import com.example.meetingroombooking.helper.ClickUtil;
import com.example.meetingroombooking.domain.MyTime;
import com.example.meetingroombooking.helper.DateUtils;
import com.example.meetingroombooking.helper.FileHelper;
import com.example.meetingroombooking.helper.LogUtils;
import com.example.meetingroombooking.helper.MeetingViewModel;
import com.example.meetingroombooking.helper.SpaceViewModel;
import com.example.meetingroombooking.login.BaseActivity;
import com.example.meetingroombooking.login.LoginActivity;
import com.example.meetingroombooking.receiver.NetBroadcastReceiver;
import com.example.meetingroombooking.helper.NetUtil;
import com.example.meetingroombooking.helper.QRCodeUtil;
import com.example.meetingroombooking.interf.CountDownForUser;
import com.example.meetingroombooking.interf.CountDownTimeInter;
import com.example.meetingroombooking.receiver.AlarmReceiver;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.lang.ref.WeakReference;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends BaseActivity implements View.OnClickListener, AutoButton.HydropowerListener, AutoButton.SoftFloorListener,
    CountDownTimeInter,NetBroadcastReceiver.NetEvevt,CountDownForUser{
    //view
    private RecyclerView recyclerView_meeting;
    private TextView button_detail;
    private RelativeLayout relativeLayout_vacant;
    private LinearLayout layout_meeting_list;
    private LinearLayout linearLayout_left;
    private RelativeLayout relativeLayout_all;
    private TextView textView_head;
    private String textView_head_content;
    private TextView logo_select;
    private TextView textView_qrCode;
    private AutoButton btn_switchbutton;
    private ImageView imageView_vacant;
    private RelativeLayout layout_time_vacant;
    private TextView textView_head_vacant;
    private ScrollView scroll_meeting;
    private TextView textView_date;
    private TextView textView_time;
    private TextView textView_week;
    private TextView textView_by;
    private LinearLayout layout_meetings;
    private TextView textView_nextMeeting;
    private TextView textView_detail_return;
//    private Spinner spinner_space;
    private Spinner spinner_meetingRoom;
    String spacename;
    String meetingRoomname;
    PopupWindow popupWindow_selectRoom;
    private TextView textView_submit;
    private LinearLayout layout_bar;
    private HorizontalScrollView horizontal_Scroll;
    private ImageView imageView_left;
    private ImageView imageView_right;
    View view_selectRoom;
    //变量
    //需要计算一个初始值
    private boolean isOccupied;
    private boolean isCN;
    private boolean isDetailOpen;
    //字体
    private Typeface typeFaceavermedium;
    private Typeface typeFaceBlack;
    private Typeface typefaceLight;
    private Typeface typefaceRoman;

    //数据源，
    private List<Space.DataBean> spaceList = new ArrayList<>();
    private List<Meeting> meetings = new ArrayList<>();
    List<String> meetingRoomNames;
   //当前space的会议室
    List<Space.DataBean.RoomsBean> roomsList_nowspace;

    private List<Meeting> meetingsListTime = new ArrayList<>();
    private List<MeetingAndRooms.DataBean> meetingRoomsList = new ArrayList<>();

    //格式化之后的倒计时时间列表
    private List<MyTime> myTimeList = new ArrayList<MyTime>();
    private List<MyTime> myTimeListforCountDown = new ArrayList<MyTime>();
    private List<MyTime> myTimeListForUser = new ArrayList<>();
    //当前会议室会议时间列表
    List<MeetingAndRooms.DataBean.RecordsBean> meetingList = new ArrayList<>();
    //会议倒计时使用者列表
    List<MyTime> myTimeListUser = new ArrayList<>();
    //adapter
    private MeetingAdapter meetingAdapter;
    ArrayAdapter<String> spaceAdapter;
    ArrayAdapter<String> meetingRoomAdapter;
    //工具
//    private SharedPreferences sharedPreferences;
//    private SharedPreferences.Editor editor;
    private Animation animation_top;
    private Animation animation_bottom;
    private Animation animation_top_exit;
    private Animation animation_bottom_exit;
    private Animation animation_alpha;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
    CustomTimeCount customTimeCount;
    ImageView imageView_qrCode;
    TextView layout_kong;
    //当前时间变量
    private String timeNowtr;
    private Date curDate;
    //时间列表的textiew
    private TextView view;
    private TextView viewtwo;
    //格式化会议时间的变量
    private String startTime;
    private String endTime;
    MyHandler handler;
    Handler handlertwo;
    private String meetingPath;
    private Socket socket;
    public BroadcastReceiver receiver;
    private boolean netMobile;
    private TextView logo_logout;

    //初始化会议室按钮
    private Button button;
    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(400, ViewGroup.LayoutParams.WRAP_CONTENT);
    Space space;
//    Gson gson = new Gson();
    long startimelong;
    long endtimelong;
    MyTime myTime =new MyTime();
    String position;
    long mins;
    Thread thread;
    Bitmap bitmap = null;
    IntentFilter filter;
    //定时器
    AlarmManager aManager;

    @Override
    public void occupiedNow() {
        updateNowOrNextMeeting();
        customTimeCount.setCount(0);
        customTimeCount.setDate(myTimeListforCountDown);
        customTimeCount.start();
        occupied();
    }

    @Override
    public void vacantNow() {
        updateNowOrNextMeeting();
        customTimeCount.setCount(0);
        customTimeCount.setDate(myTimeListforCountDown);
        customTimeCount.start();
        vacant();
    }

    @Override
    public void onNetChange(boolean netMobile) {
        this.netMobile = netMobile;
//        Log.e("netmobileInterface",netMobile+"------------");
        isNetWork();
    }
    private void isNetWork(){
        if (netMobile){
            layout_kong.setVisibility(View.GONE);
            button_detail.setEnabled(true);
            logo_select.setEnabled(true);
            button_detail.setBackgroundResource(R.drawable.bg_detail_return);
        }else{
            linearLayout_left.setVisibility(View.GONE);
            relativeLayout_vacant.setVisibility(View.GONE);
            layout_kong.setVisibility(View.VISIBLE);
//            button_detail.setVisibility(View.GONE);
            logo_select.setEnabled(false);
            button_detail.setPressed(false);
            button_detail.setEnabled(false);
            button_detail.setBackgroundColor(Color.rgb(169,169,169));
            imageView_qrCode.setImageResource(R.drawable.booking_qrcode);
        }
        try {
            isDetailOpen = sharedPreferences.getBoolean("isDetailOpen", false);
            initAdapters();
            updataTime();
            getSpaceAndMeetingRoomNew(CommonUrl.SPACEANDROOM);
        } catch (Exception e) {
//            e.printStackTrace();
            MobclickAgent.reportError(getApplicationContext(),"252exception="+e.getMessage());
        }

        if (isCN) {
            btn_switchbutton.setmCurrent(true);
            btn_switchbutton.setLeftDis(0);
            btn_switchbutton.flushView();
        } else {
            btn_switchbutton.setmCurrent(false);
            btn_switchbutton.setLeftDis(68);
            btn_switchbutton.flushView();
        }
        textView_head.setText(meetingRoomname);
        editor.putBoolean("isDetailOpen", true);
        button_detail.setPressed(true);
        relativeLayout_all.setBackgroundColor(Color.WHITE);
    }

    @Override
    public void updateUser() {
        getMeetingNew(meetingPath,meetingroomidshare,true,false);
//        Log.e("294getmeetingtrue","========");
//        Log.e("updateUser","-------updateUser");
        button_detail.setText(getString(R.string.detail));
        long halfhour = 1000*60*30;
        UserCountDown userCountDown = new UserCountDown(halfhour+1000,60000);
        userCountDown.setCountDownTimeForUserInter(this);
        userCountDown.start();
    }

    class MyHandler extends Handler {
        private WeakReference<MainActivity> reference;

        public MyHandler(Activity activity) {
//            //使用弱引用包裹当前的activity
            reference = new WeakReference(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            MainActivity activity = reference.get();
            //当前Activity不存在或者页面正在关闭的时候，不在进行消息处理
            if (activity != null && !activity.isFinishing()) {
                if (msg.what == 1) {
                    try {
                        //实时更新界面
//                        Log.e("handler.what=1","handler.what==1");
                        updataTime();
                    } catch (Exception e) {
                        MobclickAgent.reportError(getApplicationContext(),"290exception="+e.getMessage());
                    }
                }
                else if (msg.what==5){
//                    Log.e("handlermsg","msg===5");
                    layout_bar.setEnabled(true);
                    for (int i =0;i<layout_bar.getChildCount();i++){
                        layout_bar.getChildAt(i).setEnabled(true);
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Toast.makeText(getApplicationContext(),"1.2.3patch",Toast.LENGTH_SHORT).show();
        Intent intent =new Intent(MainActivity.this, AlarmReceiver.class);
        intent.setAction("restart");
        aManager=(AlarmManager)getSystemService(Service.ALARM_SERVICE);
        PendingIntent sender=PendingIntent.getBroadcast(MainActivity.this,0, intent, 0);
        long timenow = System.currentTimeMillis();
        long l = 24*60*60*1000; //每天的毫秒数86400000
//        Log.e("shijian",(date.getTime()%l+8*60*60*1000)+"=="+(l-date.getTime()%l-8*60*60*1000));
        aManager.set(AlarmManager.RTC,timenow+(l-(timenow%l+8*60*60*1000)),sender);
//        NetUtil.getDeviceInfo(getApplicationContext());
        //这是友盟设置是否对日志信息进行加密, 默认false(不加密).
        MobclickAgent.enableEncrypt(true);//6.0.0版本以前
        MobclickAgent.setCatchUncaughtExceptions(true);
        //通过代码的方式动态注册MyBroadcastReceiver
         receiver=new NetBroadcastReceiver(this);
//       这里可以写系统的广播接收者重写onReceiver方法就可以)
        filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        //注册receiver
        registerReceiver(receiver, filter);
        Resources resources = MainActivity.this.getResources();
        handler = new MyHandler(MainActivity.this);
        handlertwo  = new Handler();

        try {
             socket = IO.socket(CommonUrl.SOCKETURL);
            /**
             * socket开关，
             * */
            socket.connect();
            socket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            socket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
    //        socket.on("eventCheckin", eventCheckin);
    //        socket.on("eventRegister", eventRegister);
            socket.on("meetingroomRecord",meetingroomRecord);

        } catch (URISyntaxException e) {
            LogUtils.e("Socketexception","socket"+e.getMessage());
        }
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = getSetLocale();
        resources.updateConfiguration(configuration, dm);

        setContentView(R.layout.activity_main);
        initView();
        timenow = System.currentTimeMillis();
        long halfhour = 1000*60*30;
        long countdown = halfhour-timenow%halfhour;
        UserCountDown userCountDown = new UserCountDown(countdown,60000);
        userCountDown.setCountDownTimeForUserInter(this);
        userCountDown.start();
        netMobile = NetUtil.isNetworkAvailable(getApplicationContext());
        meetingRoomNames = new ArrayList<>();
        spaceidshare = sharedPreferences.getString("spaceID","5876e6ee666c49e806f46636");
//        Log.e("倒计时","----------"+countdown);
        LogUtils.e("main-spaceidshare==",spaceidshare);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        count=0;
        registerReceiver(receiver, filter);
        LogUtils.e("onRestart","---------------------");
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void initView() {
        logo_logout = (TextView) findViewById(R.id.logo_logout);
        logo_logout.setOnClickListener(this);
        layout_kong = (TextView) findViewById(R.id.layout_kong);
        layout_kong.setTypeface(typeFaceBlack);
        imageView_qrCode = (ImageView) findViewById(R.id.imageView_qrCode);
        customTimeCount = (CustomTimeCount) findViewById(R.id.RushBuyCountDownTimerView);
        animation_top = AnimationUtils.loadAnimation(this, R.anim.enter_top);
        animation_bottom = AnimationUtils.loadAnimation(this, R.anim.enter);
        animation_alpha = AnimationUtils.loadAnimation(this, R.anim.enter_alpha);
        animation_bottom_exit = AnimationUtils.loadAnimation(this, R.anim.exit);
        animation_top_exit = AnimationUtils.loadAnimation(this, R.anim.exit_top);

        typeFaceavermedium = Typeface.createFromAsset(getAssets(), "Avenir-Medium.otf");
        typeFaceBlack = Typeface.createFromAsset(getAssets(), "Avenir-Black.ttf");
        typefaceLight = Typeface.createFromAsset(getAssets(), "Avenir Next LT W01 Ultra Light.ttf");
        typefaceRoman = Typeface.createFromAsset(getAssets(),"Avenir-Roman.otf");

        recyclerView_meeting = (RecyclerView) findViewById(R.id.recycler_meeting);
        scroll_meeting = (ScrollView) findViewById(R.id.scroll_meeting);
        scroll_meeting.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        button_detail = (TextView) findViewById(R.id.textView_detail_return);
        button_detail.setTypeface(typeFaceavermedium);
        button_detail.setOnClickListener(this);
        layout_meeting_list = (LinearLayout) findViewById(R.id.layout_meetings);
        relativeLayout_vacant = (RelativeLayout) findViewById(R.id.layout_left_vacant);
        linearLayout_left = (LinearLayout) findViewById(R.id.layout_left);
        relativeLayout_all = (RelativeLayout) findViewById(R.id.layout_all);
        textView_head = (TextView) findViewById(R.id.textView_head);
        textView_head.setTypeface(typeFaceBlack);
        logo_select = (TextView) findViewById(R.id.logo_select);
        logo_select.setOnClickListener(this);
        textView_qrCode = (TextView) findViewById(R.id.textView_qrCode);
        textView_qrCode.setTypeface(typeFaceBlack);
        button_detail.setTypeface(typeFaceavermedium);
        //中英切换按钮
        btn_switchbutton = (AutoButton) findViewById(R.id.autoButton);
        btn_switchbutton.setHydropowerListener(this);
        btn_switchbutton.setSoftFloorListener(this);
        imageView_vacant = (ImageView) findViewById(R.id.imageView_vacant);
        layout_time_vacant = (RelativeLayout) findViewById(R.id.layout_time_vacant);
        textView_head_vacant = (TextView) findViewById(R.id.textView_head_vacant);
        textView_head_vacant.setTypeface(typeFaceavermedium);

        //时间控件
        textView_date = (TextView) findViewById(R.id.textView_date);
        textView_time = (TextView) findViewById(R.id.textView_time);
        textView_week = (TextView) findViewById(R.id.textView_week);
        textView_date.setTypeface(typefaceRoman);
        textView_time.setTypeface(typefaceRoman);
        textView_week.setTypeface(typefaceRoman);

        textView_by = (TextView) findViewById(R.id.textView_by);
        textView_by.setTypeface(typefaceRoman);
        textView_nextMeeting= (TextView) findViewById(R.id.textView_nextMeeting);
        textView_nextMeeting.setTypeface(typeFaceavermedium);
        layout_meetings = (LinearLayout) findViewById(R.id.layout_meetings);

        view_selectRoom = LayoutInflater.from(this).inflate(R.layout.select_room,null);
        view_selectRoom.setFocusableInTouchMode(true);
        popupWindow_selectRoom = new PopupWindow(view_selectRoom,1500,800);
        //设置popWindow
        popupWindow_selectRoom.setBackgroundDrawable(new ColorDrawable(Color.GRAY));
        popupWindow_selectRoom.setFocusable(false);
        popupWindow_selectRoom.setOutsideTouchable(true);
        popupWindow_selectRoom.setAnimationStyle(R.style.popWindow_animation);

//        spinner_space = (Spinner)view_selectRoom.findViewById(R.id.spinner_space);
        spinner_meetingRoom = (Spinner)view_selectRoom.findViewById(R.id.spinner_meetingRoom);
        textView_submit = (TextView) view_selectRoom.findViewById(R.id.textView_submit);
        textView_submit.setOnClickListener(this);
        layout_bar = (LinearLayout) findViewById(R.id.layout_bar);
        horizontal_Scroll = (HorizontalScrollView) findViewById(R.id.horizontal_Scroll);
        imageView_left = (ImageView) findViewById(R.id.imageView_left);
        imageView_right = (ImageView) findViewById(R.id.imageView_right);

//        int x =(int) horizontal_Scroll.getX()+horizontal_Scroll.getWidth();
//        horizontal_Scroll.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged() {
//                View childView = horizontal_Scroll.getChildAt(0);
//                int hight= childView.getWidth()+view.getScrollX();
//                if (childView  != null && childView .getMeasuredWidth() == view.getScrollX() + view.getWidth()){
//                    imageView_right.setImageBitmap(null);
//                    imageView_right.setBackgroundResource(R.drawable.slide_left_click);
//                    imageView_left.setImageBitmap(null);
//                }else if (childView  != null && childView .getMeasuredWidth() > view.getScrollX() + view.getWidth()){
//                    if (view.getScrollX()==0){
//                        imageView_right.setImageBitmap(null);
//                        imageView_left.setImageBitmap(null);
//                        imageView_left.setBackgroundResource(R.drawable.slide_right_click);
//                    }else{
//                    imageView_right.setImageBitmap(null);
//                    imageView_right.setBackgroundResource(R.drawable.slide_left_click);
//                    imageView_left.setImageBitmap(null);
//                    imageView_left.setBackgroundResource(R.drawable.slide_right_click);
//                    }
//                }
//            }
//        });
    }

    String spaceidshare;//目前正处于登陆状态的spaceid
    int meetingroomidshare;
    int spaceindex;
    private void getQrCOde(final String url){
        final String filePath = QRCodeUtil.getFileRoot(getApplicationContext()) + File.separator
                + "qr_" + System.currentTimeMillis() + ".jpg";
         thread = new Thread(new Runnable() {
            @Override
            public void run() {
                boolean success = QRCodeUtil.createQRImage(url, 800, 800, filePath);
                if (success) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                // 实例化Bitmap
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inSampleSize=2;
                                bitmap = BitmapFactory.decodeFile(filePath,options);
                                imageView_qrCode.setImageBitmap(bitmap);
                            } catch (OutOfMemoryError e) {
                                imageView_qrCode.setImageResource(R.drawable.booking_qrcode);
                            }
                        }
                    });
                }
            }
        });
        thread.start();
        FileHelper.delJPGFiles(QRCodeUtil.getFileRoot(getApplicationContext()));
    }
    private void setSpaceandRoom(){
        //从本地取出最新数据设置
        spacename = sharedPreferences.getString("spacename"," ");
        meetingroomidshare= sharedPreferences.getInt("meetingroomid",0);
        meetingRoomname = sharedPreferences.getString("meetingRoomname",meetingRoomNames.get(0));
        textView_head.setText(meetingRoomname);
        button_detail.setText(getString(R.string.detail));
//        Log.e("roomlist_nowspace===",roomsList_nowspace.toString());
        //删除空间的时候会用到，等待测试
//        if (spaceidshare>=spaceSize){
//        int meetingRoomsSize = spaceList.get(spaceindex).getRooms().size();
//            spaceidshare=0;
//            editor.putInt("spaceid",0);
//            editor.commit();
//            if (spaceSize!=0){
//                editor.putString("space",spaceList.get(0).getName());
//                editor.commit();
//                textView_head.setText(spaceList.get(0).getName());
//                if (meetingroomidshare>=meetingRoomsSize){
//                    meetingroomidshare = 0;
//                    editor.putInt("meetingroomid",0);
//                    editor.commit();
//                    if (meetingRoomsSize!=0){
//                        editor.putString("meetingRoomname",(spaceList.get(spaceindex).getRooms().get(meetingroomidshare).getName()));
//                    }
//                }

        //更新可点击按钮
     if (roomsList_nowspace.size()!= 0){
         meetingPath = CommonUrl.MEETINGS+spaceidshare;
         getMeetingNew(meetingPath,meetingroomidshare,true,true);

        layout_bar.removeAllViews();
        layoutParams.setMargins(20,0,20,0);
        int roomSize = roomsList_nowspace.size()-1;
        for (int i =roomSize;i>=0;i--){
            button = new Button(getApplicationContext());
            button.setId(i);
            button.setTag(i);
            button.setLayoutParams(layoutParams);
            button.setText((roomsList_nowspace.get(i).getName()));
            button.setTypeface(typeFaceBlack);
            button.setTextColor(Color.rgb(242, 146, 39));
            button.setBackgroundResource(R.drawable.bg_bt_room);
            button.setSelected(false);
            button.setTextSize(TypedValue.COMPLEX_UNIT_PX,43);
            button.setGravity(Gravity.CENTER);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    layout_bar.setEnabled(false);
                    int indexOfbutton = layout_bar.indexOfChild(view);
                    try {
                        //判断会议室被删除的情况，用当前id去判断在不在本地id中？？但其实本地数据组也都没有缓存，请求不到数据出弹窗吧
                        if (meetingRoomsList.size()-1>=indexOfbutton) {
                            meetingList = meetingRoomsList.get(indexOfbutton).getRecords();
                            upDateMeetingDate();
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    view.setSelected(true);
                    //友盟
                    MobclickAgent.onEvent(getApplicationContext(),"roomCLick");
                    LogUtils.e("tabOnclick",view.getTag());
                    int i = (int)view.getTag();
                    //滚动条滚到中间点的位置
                    horizontal_Scroll.smoothScrollTo(view.getLeft() - (1330 / 2 - view.getWidth() / 2), 0);
                    Button buttonlayoutBar;
                    int laybarcount = layout_bar.getChildCount();
                    for (int m =0;m<laybarcount;m++){
                        buttonlayoutBar = (Button) layout_bar.getChildAt(m);
                        if (i!=m){
                            buttonlayoutBar.setSelected(false);
                            buttonlayoutBar.setTextColor(Color.rgb(242, 146, 39));
                        }
                    }
                    ((Button)view).setTextColor(Color.WHITE);
                }
            });
            layout_bar.addView(button,0);
        }
     }
        try{
            if (layout_bar.getChildCount()<=3){
                imageView_right.setImageBitmap(null);
                imageView_left.setImageBitmap(null);
            }else{
                imageView_left.setImageBitmap(null);
                imageView_left.setBackgroundResource(R.drawable.slide_right_click);
                imageView_right.setImageBitmap(null);
                imageView_right.setBackgroundResource(R.drawable.slide_left_click);
            }
                ((Button)layout_bar.getChildAt(meetingroomidshare)).setTextColor(Color.rgb(255, 255, 255));
                (layout_bar.getChildAt(meetingroomidshare)).setSelected(true);
        }catch (Exception e){
            MobclickAgent.reportError(getApplicationContext(),"597Exception="+e.getMessage());
//            LogUtils.e("bitmapException",e.getMessage().toString());
        }
    }

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MobclickAgent.onEvent(getApplicationContext(),"SocketConnectedFailure");
                    Toast.makeText(MainActivity.this,
                            "Socket is unavailable", Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private Emitter.Listener meetingroomRecord = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    Log.e("socket内容====",args[0].toString()+args[0].getClass().getName());
                    String space_id = sharedPreferences.getString("space_id","5876e6ee666c49e806f46636");
                    //友盟点击事件
                    MobclickAgent.onEvent(getApplicationContext(),"SocketReceived");
                    if(String.valueOf(space_id).equals(args[0].toString())){
                        String path = CommonUrl.MEETINGS+args[0];
                        for (int m =0;m<layout_bar.getChildCount();m++){
                            Button buttonlayoutBar = (Button) layout_bar.getChildAt(m);
                            buttonlayoutBar.setSelected(false);
                            buttonlayoutBar.setTextColor(Color.rgb(242, 146, 39));
                        }
                        ((Button)(layout_bar.getChildAt(meetingroomidshare))).setTextColor(Color.WHITE);
                        layout_bar.getChildAt(meetingroomidshare).setSelected(true);
                        getMeetingNew(path,meetingroomidshare,true,false);
                    }else{
//                        Log.e("SocketPath不是本地点=======",CommonUrl.MEETINGS+args[0]);
                    }

                }
            });
        }
    };
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textView_detail_return:
                //选择之前，确定哪个按钮
                if (ClickUtil.isFastDoubleClick()) {
                    break;
                } else {
                    if (imageView_vacant.isShown()){
//                        LogUtils.e("clickDetail","clickDetailButton");
                        Button buttonlayoutBar;
                        for (int m =0;m<layout_bar.getChildCount();m++){
                            buttonlayoutBar = (Button) layout_bar.getChildAt(m);
                            buttonlayoutBar.setSelected(false);
                            buttonlayoutBar.setTextColor(Color.rgb(242, 146, 39));
                        }
                        try{
                            if (layout_bar.getChildCount()-1>=meetingroomidshare){
                                ((Button)layout_bar.getChildAt(meetingroomidshare)).setTextColor(Color.rgb(255, 255, 255));
                                 (layout_bar.getChildAt(meetingroomidshare)).setSelected(true);
                                getMeetingNew(meetingPath,meetingroomidshare,false,false);
                                textView_head.setText(spacename);
                            }
                        //友盟的点击事件统计
                        MobclickAgent.onEvent(getApplicationContext(),"clickDetail");
                        editor.putBoolean("isDetailOpen", false);
                        editor.commit();
                        relativeLayout_all.setBackgroundColor(Color.rgb(241, 244, 246));
                        linearLayout_left.startAnimation(animation_alpha);
                        linearLayout_left.setVisibility(View.VISIBLE);
                        layout_time_vacant.startAnimation(animation_bottom_exit);
                        layout_time_vacant.setVisibility(View.INVISIBLE);
                        imageView_vacant.startAnimation(animation_top_exit);
                        imageView_vacant.setVisibility(View.INVISIBLE);
                        textView_by.setVisibility(View.INVISIBLE);
                        textView_by.startAnimation(animation_top_exit);
                        button_detail.setText(R.string.back);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }else{
                        LogUtils.e("clickReturn","clickReturnButton");
                        MobclickAgent.onEvent(getApplicationContext(),"clickReturn");
                        editor.putBoolean("isDetailOpen", true);
                        editor.commit();
                        button_detail.setPressed(true);
                        textView_head_content = sharedPreferences.getString("meetingRoomname",getString(R.string.roomatitle));
                        textView_head.setText(textView_head_content);
                        relativeLayout_all.setBackgroundColor(Color.WHITE);
                        if (myTimeListforCountDown.size()>0){
                            isOccupied = myTimeListforCountDown.get(0).isOccupied();
                        }
                        if (isOccupied) {
                            occupied();
                        } else {
                            vacant();
                        }
                        button_detail.setText(R.string.detail);
                    }
                }
                break;

            case R.id.logo_select:
                getSpaceAndMeetingRoomNew(CommonUrl.SPACEANDROOM);
                popupWindow_selectRoom.showAtLocation(view_selectRoom,Gravity.CENTER,Gravity.CENTER_HORIZONTAL,Gravity.CENTER_HORIZONTAL);
                break;
            case R.id.textView_submit:
                //记录选择了哪个空间和会议室
//                if (spaceList.size()==0||meetingRoomsList.size()==0){
//                    Toast.makeText(getApplicationContext(),"会议室不存在，请重新选择",Toast.LENGTH_LONG).show();
//                } else {
                    setSpaceandRoom();
                    HashMap<String,String> map = new HashMap<String,String>();
                    map.put("space",spacename);
                    map.put("meetingRoomname",meetingRoomname);
                    MobclickAgent.onEvent(getApplicationContext(),"chooseRoomSubmit",map);
                    popupWindow_selectRoom.dismiss();
                    break;
//                }
            case R.id.logo_logout:
                Intent intentLogin = new Intent(getApplicationContext(),LoginActivity.class);
                clear();
                startActivity(intentLogin);
                break;


        }
    }
    public void clear(){
        //退出，清除本地token保存，下一次重新登陆；
        editorLogin.putString("token","");
        editorLogin.commit();
        editorLogin.putString("spaceID","");
        editorLogin.commit();
        editorLogin.putString("slogan","");
        editorLogin.commit();
        editorLogin.putString("topLeftLogo","");
        editorLogin.commit();
        editorLogin.putString("logo","");
        editorLogin.commit();
        editorLogin.putString("spaceBean","");
        editorLogin.commit();
    }
    int count =0;
    private void updataTime() throws Exception {
        long time = System.currentTimeMillis();
        Date data = new Date(time);
        if (isCN) {
            textView_time.setText(DateUtils.getDate(data, "HH:mm",Locale.CHINA));
            textView_date.setText(DateUtils.getDate(data, "MMMdd",Locale.CHINA));
            textView_week.setText(DateUtils.getDate(data, "EEEE",Locale.CHINA));
        } else {
            textView_time.setText(DateUtils.getDate(data, "HH:mm", Locale.ENGLISH));
            textView_date.setText(DateUtils.getDate(data, "dd MMM", Locale.ENGLISH));
            textView_week.setText(DateUtils.getDate(data, "EEE", Locale.ENGLISH));
        }
        if (count==0){
            String timenow = DateUtils.getDate(data, "HHmmss");
            int start = 60000-Integer.parseInt(timenow.substring(4))*1000;
            handler.sendEmptyMessageDelayed(1, start);
            count++;
        }else{
            if(handler.hasMessages(1)){
            }else{
                handler.sendEmptyMessageDelayed(1, 60000);
            }
        }
    }

    private void initAdapters() throws ParseException {
        meetingAdapter = new MeetingAdapter(getApplicationContext());
        recyclerView_meeting.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        recyclerView_meeting.setAdapter(meetingAdapter);
    }

    long currentTimeLong;
    String combinedTime;

    //主要的计算时间间隔的方法，包括了计算当前时间所在的区间段，
    private void initCountDownTime() {
        curDate = new Date(System.currentTimeMillis());//获取当前时间
        timeNowtr = formatter.format(curDate);
        myTimeList.clear();
        myTimeListforCountDown.clear();
        myTimeListForUser.clear();
        try {
            for (int i = 0; i < meetingList.size(); i++) {
                    startimelong = Long.parseLong(meetingList.get(i).getStartTime());
                    endtimelong = Long.parseLong(meetingList.get(i).getEndTime());
                    myTime = new MyTime(startimelong,endtimelong,meetingList.get(i).getTitle());
                    myTimeList.add(myTime);
            }
             currentTimeLong = Long.parseLong(timeNowtr);
            if (myTimeList.size()>0){
                //新的时间间隔，倒计时的数组
                myTimeListforCountDown.clear();
                myTimeListforCountDown = MyTime.CountTime(myTimeList, currentTimeLong, currentTimeLong, meetings);
                myTimeListForUser=MyTime.CountTimeForUsers(myTimeList, currentTimeLong, currentTimeLong, meetings);
            }else{
                if (meetings.size()>0){
                    startimelong = Long.parseLong(meetings.get(0).getStartTime());
                    endtimelong = Long.parseLong(meetings.get(meetings.size()-1).getEndTime());
                    myTime = new MyTime(startimelong, endtimelong,"");
                    myTimeList.add(myTime);
                    myTimeListforCountDown.clear();
                    myTimeListforCountDown=MyTime.CountTimeNoMeeting(myTimeList,currentTimeLong,currentTimeLong);
                }
            }
//            LogUtils.e("myTimeListforCountDown",myTimeListforCountDown.toString());
//            LogUtils.e("myTimeListForUser",myTimeListForUser.toString());
        } catch (Exception e) {
            MobclickAgent.reportError(getApplicationContext(),"initCountDownTime="+e.getMessage());
            LogUtils.e("initCountDownTime","exception"+e.getMessage());
        }
    }
    //刷新会议界面
    private void upDateMeetingDate() throws ParseException {
        //先初始化
//        LogUtils.e("updateMeetingDate","刷新了会议室界面");
        int size = layout_meeting_list.getChildCount();
        LinearLayout linearLayout;
        for (int m =0;m < size ;m++){
            linearLayout = (LinearLayout) layout_meeting_list.getChildAt(m);
            linearLayout.setVisibility(View.VISIBLE);
            linearLayout.setBackgroundColor(Color.WHITE);
            ((TextView)(linearLayout).getChildAt(0)).setText("");
        }
        int start = 0;
        MeetingAndRooms.DataBean.RecordsBean meeting;
        Meeting meetingcompare;
        int meetingNeedtoAdd = (layout_meeting_list.getChildCount()) - meetings.size() * 2;
        //隐藏不该显示的条目
        if (meetingNeedtoAdd >= 0) {
//                int size = layout_meeting_list.getChildCount();
            for (int i = meetings.size() * 2; i < size; i++){
                layout_meeting_list.getChildAt(i).setVisibility(View.GONE);
            }
        }
        int meetingListSize = meetingList.size();
        for (int i = 0; i < meetingListSize; i++) {
            meeting = meetingList.get(i);
            //应该计算时间格式，不应该是int。
             mins = DateUtils.toMins(meeting.getEndTime(), meeting.getStartTime());
             int n = (int) (mins / 30);
             position = meeting.getStartTime().substring(8, 12);
             meeting.getEndTime();
                for (int p = 0; p < meetingsListTime.size(); p++) {
                    meetingcompare = meetingsListTime.get(p);
                    if (meetingcompare.getStartTime().substring(8, 12).equals(position)) {
                        start = p;
                        break;
                    }
                }
            startTime = meeting.getStartTime().substring(8, 10) + ":" + meeting.getStartTime().substring(10, 12);
            endTime = meeting.getEndTime().substring(8, 10) + ":" + meeting.getEndTime().substring(10, 12);
            combinedTime = startTime + "~" + endTime;
                view = ((TextView) (((LinearLayout) (layout_meeting_list.getChildAt(start))).getChildAt(0)));
                view.setTypeface(typeFaceBlack);
                view.setTextSize(TypedValue.COMPLEX_UNIT_PX, 40);
                view.setText(meeting.getTitle() + "  " + combinedTime);
                if (meeting.getStatus()) {
                    view.setTextColor(Color.rgb(242, 146, 39));
                } else {
                    view.setTextColor(Color.rgb(105, 105, 105));
                }
            LinearLayout  linearLayouttwo;
            for (int m = start; m < n + start; m++) {
                linearLayouttwo = (LinearLayout) layout_meeting_list.getChildAt(m);
                if (meeting.getStatus()) {
                    linearLayouttwo.setBackgroundColor(Color.argb(127, 242, 146, 39));
                } else {
                    linearLayouttwo.setBackgroundColor(Color.argb(153, 205, 205, 205));
                }
            }
        }
    }

    private void updateNowOrNextMeeting() {
    //更新首页当前会议和下个会议
        initCountDownTime();
//        Log.e("updateNowOrNextMeeting","updateNowOrNextMeeting");
        if (myTimeListforCountDown.size()>0){
            isOccupied = myTimeListforCountDown.get(0).isOccupied();
        }else{
            isOccupied = false;
            textView_by.setVisibility(View.GONE);
            textView_by.setText("");
            textView_nextMeeting.setText(getString(R.string.nextMeeting)+R.string.meetingover);
        }
    if (isOccupied){
//        long timecount= 0;
//        try {
//            timecount = DateUtils.dataSubReturnLong(myTimeListForUser.get(0).getEnd()+"",myTimeListForUser.get(0).getStart()+"");
//            UserCountDown userCountDown = new UserCountDown(timecount,60000);
//            userCountDown.setCountDownTimeForUserInter(this);
//            Log.e("updateUser","-------"+timecount);
//            userCountDown.start();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        if (myTimeListForUser.size()>=2){
            if (myTimeListForUser.get(0).getCompany().length()==0){
                textView_by.setText(getString(R.string.notOpen));
            }else{
                textView_by.setText("by"+"\n"+myTimeListForUser.get(0).getCompany());
            }
            startTime = (myTimeListForUser.get(1).getStart()+"").substring(8, 10) + ":" +
                    (myTimeListForUser.get(1).getStart()+"").substring(10, 12);
            endTime =( myTimeListForUser.get(1).getEnd()+"").substring(8, 10) + ":" +
                    (myTimeListForUser.get(1).getEnd()+"").substring(10, 12);
            textView_nextMeeting.setText(getString(R.string.nextMeeting)+" "+
                    myTimeListForUser.get(1).getCompany()+" "+startTime+"~"+endTime);
        }else if (myTimeListForUser.size()==1){
            textView_by.setText("by"+"\n"+myTimeListForUser.get(0).getCompany());
            textView_nextMeeting.setText(R.string.meetingover);
        }else if(myTimeListForUser.size()==0){
            textView_by.setText(getString(R.string.notOpen));
            textView_nextMeeting.setText(R.string.meetingover);

        }
    }else{
        textView_by.setVisibility(View.GONE);
        textView_by.setText("");
        try{
            startTime = (myTimeListForUser.get(0).getStart()+"").substring(8, 10) + ":" + (myTimeListForUser.get(0).getStart()+"").substring(10, 12);
            endTime =( myTimeListForUser.get(0).getEnd()+"").substring(8, 10) + ":" + (myTimeListForUser.get(0).getEnd()+"").substring(10, 12);
            textView_nextMeeting.setText(getString(R.string.nextMeeting)+" "+myTimeListForUser.get(0).getCompany()+" "+startTime+"~"+endTime);
        }catch (Exception e){
            textView_nextMeeting.setText(R.string.meetingover);
        }
        LogUtils.e("isOccupied","isOccupied="+isOccupied);
        LogUtils.e("textBy","by="+textView_by.getText().toString());
        LogUtils.e("textNext","nextmeeting="+textView_nextMeeting.getText().toString());
    }
}

    Meeting meeting ;//半小时上半小时
    Meeting meeting2;//半小时后半小时
    Meeting meeting3;//一整个小时
    private void getMeetingNew(String path, final int meetingroomid, final boolean upDateCountTime,final boolean upDateQRcode){
//        for (int i =0;i<layout_bar.getChildCount();i++){
//            layout_bar.getChildAt(i).setEnabled(false);
//        }

        MeetingViewModel meetingViewModel = new MeetingViewModel();
        if (!TextUtils.isEmpty(spaceidshare)){
            final Call currentEventCall = meetingViewModel.getMeeting(spaceidshare);
            currentEventCall.enqueue(new Callback<MeetingAndRooms>() {
            @Override
            public void onResponse(Call<MeetingAndRooms> call, retrofit2.Response<MeetingAndRooms> response) {
                MeetingAndRooms meetingAndRooms = response.body();
                int meetingroomidnew;
                if (meetingAndRooms.getCode()==0 && meetingAndRooms!=null){
                    meetingRoomsList = meetingAndRooms.getData();
                    if (meetingRoomsList.size()!=0&&meetingRoomsList!=null){
                        if (meetingroomid>=meetingRoomsList.size()){
                            meetingroomidnew=0;
                            editor.putInt("meetingroomid",0);
                            editor.commit();
                            textView_head.setText(meetingRoomsList.get(spaceindex).getName());
                        }else{
                            meetingroomidnew = meetingroomid;
                        }
                        meetingList = meetingRoomsList.get(meetingroomidnew).getRecords();
                        MeetingAndRooms.DataBean data = meetingRoomsList.get(meetingroomidnew);
                        int start = Integer.parseInt(data.getOpenTime());
                        int end = Integer.parseInt(data.getCloseTime());
                        String date = DateUtils.getCurrentDate("yyyyMMdd");
                        if (meetings.size()==0){

                            for (int i = start; i < end; i++) {
                                meeting = new Meeting(date + DateUtils.formatNumber(i) + "0000", date + DateUtils.formatNumber(i) + "3000");
                                meeting2 = new Meeting(date + DateUtils.formatNumber(i) + "3000", date + DateUtils.formatNumber(i + 1) + "0000");
                                meetingsListTime.add(meeting);
                                meetingsListTime.add(meeting2);
                                meeting3 = new Meeting(date + DateUtils.formatNumber(i) + "0000", date + DateUtils.formatNumber(i + 1) + "0000");
                                meetings.add(meeting3);
                            }
                        MobclickAgent.reportError(getApplicationContext(),"meetings="+meetings.toString());
                        meetingAdapter.setData(meetings);
                            LogUtils.e("getmeetingnew=meetings=",meetings.toString());
                        }
                        try {
                            scroll_meeting.setVisibility(View.VISIBLE);
                            //刷新会议列表
                            upDateMeetingDate();
                            if (upDateCountTime){
                                updateNowOrNextMeeting();
                                //倒计时的时间变化
                                if (myTimeListforCountDown.size()>0){
                                    isOccupied = myTimeListforCountDown.get(0).isOccupied();
                                }else{
                                    isOccupied=false;
                                }
                                if (isOccupied) {
                                    occupied();
                                } else {
                                    vacant();
                                }
                                customTimeCount = (CustomTimeCount) findViewById(R.id.RushBuyCountDownTimerView);
                                customTimeCount.setCountDownTimeInter(MainActivity.this);
                                customTimeCount.setCount(0);
                                customTimeCount.setDate(myTimeListforCountDown);
                                customTimeCount.start();
                            }
                            if (upDateQRcode){
                                    imageView_qrCode.setImageResource(R.drawable.booking_qrcode);
                                if (isCN){
                                    getQrCOde(CommonUrl.QRCODEHEAFD+meetingRoomsList.get(meetingroomidshare).get_id()+CommonUrl.QRCODEEND);
                                }else{
                                    getQrCOde(CommonUrl.ENQRCODEHEAD+meetingRoomsList.get(meetingroomidshare).get_id()+CommonUrl.ENQRCODEEND);
                                }
                            }
                            if (!handler.hasMessages(5)){
                                handler.sendEmptyMessage(5);
                            }
                        } catch (Exception e) {
                            MobclickAgent.reportError(getApplicationContext(),"getMeetingNew="+e.getMessage());
                            LogUtils.e("getMeetingNew","exception"+e.getMessage());
                        }
                    }
                }else{
                    scroll_meeting.setVisibility(View.GONE);
                    layout_kong.setVisibility(View.VISIBLE);
                }
                meeting=null;
                meeting2=null;
                meeting3=null;

            };
            @Override
            public void onFailure(Call<MeetingAndRooms> call, Throwable t) {
                MobclickAgent.reportError(getApplicationContext(),"getMeetingNewOnfailure="+t.getMessage());

            }
        });
        }
    }
    int meetingroomindex;
    int countSetselection = 0;
    private void getSpaceAndMeetingRoomNew(String path){
        SpaceViewModel spaceViewModel = new SpaceViewModel();
        Call<Space> spaceAndMaeetingRoomsCall = spaceViewModel.getSpaceAndMaeetingRooms();
        spaceAndMaeetingRoomsCall.enqueue(new Callback<Space>() {
            @Override
            public void onResponse(Call<Space> call, retrofit2.Response<Space> response) {
                try {
                    space = response.body();
                    LogUtils.e("getSpaceAndMeetingRoomNew====",space.toString());
                    if (space.getCode() == 0 && space != null) {
                        spaceList = space.getData();
                        int spacesize = spaceList.size();
                        if (spacesize!= 0) {
                            meetingRoomNames.clear();
                            for (int n = 0;n<spacesize;n++){
                                if (spaceList.get(n).get_id().equals(spaceidshare)){
                                    spaceindex = n;
                                    spacename = spaceList.get(n).getName();
                                    editor.putString("spacename",spacename);
                                    editor.commit();
                                }
                            }
                            roomsList_nowspace = spaceList.get(spaceindex).getRooms();
                            int meetingRoomsSize=roomsList_nowspace.size();
                            for (int m = 0; m < meetingRoomsSize; m++) {
                                meetingRoomNames.add(roomsList_nowspace.get(m).getName());
                            }
                            meetingRoomAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, meetingRoomNames);
                            meetingRoomAdapter.setDropDownViewResource(R.layout.select_dialog_item_new);
                            spinner_meetingRoom.setAdapter(meetingRoomAdapter);
                            spinner_meetingRoom.setSelection(meetingroomindex, true);
                            setSpaceandRoom();

                            spinner_meetingRoom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    if (meetingRoomNames.size()!=0 && spaceList.size() != 0){
                                        meetingroomindex = spinner_meetingRoom.getSelectedItemPosition();
                                        editor.putString("meetingRoomname", meetingRoomNames.get(meetingroomindex));
                                        editor.commit();
                                        editor.putInt("meetingroomid", meetingroomindex);
                                        editor.commit();
                                        editor.putString("meetingroom_id", spaceList.get(spaceindex).getRooms().get(meetingroomindex).get_id());
                                        editor.commit();
                                    }else {
                                        Toast.makeText(getApplicationContext(),"会议室不存在",Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {
                                    if (meetingRoomNames.size() != 0 && spaceList.size() != 0) {
                                        editor.putString("meetingRoomname", meetingRoomNames.get(0));
                                        editor.commit();
                                        editor.putInt("meetingroomindex", 0);
                                        editor.commit();
                                        editor.putString("meetingroom_id", spaceList.get(spaceindex).getRooms().get(meetingroomindex).get_id());
                                        editor.commit();
//                                Log.e("1选择的room",sharedPreferences.getString("meetingroom","没有存上room")+sharedPreferences.getInt("meetingroomid",222));
                                    }else {
                                        Toast.makeText(getApplicationContext(),"会议室不存在",Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }
                    }
                } catch (Exception e) {
//                    Log.e("SpaceJsonException",e.getMessage()+"=1249");
                }
            }

            @Override
            public void onFailure(Call<Space> call, Throwable t) {
                MobclickAgent.reportError(getApplicationContext(),"getSpaceOnfailure="+t.getMessage());
            }
        });
    }

    private void occupied() {
//        Log.e("occupied", "occupied");
        isOccupied = true;
        button_detail.setText(getString(R.string.detail));
        textView_head.setText(meetingRoomname);
        linearLayout_left.setVisibility(View.GONE);
        relativeLayout_vacant.setVisibility(View.VISIBLE);
        imageView_vacant.setBackgroundResource(R.drawable.occupied);
        imageView_vacant.setVisibility(View.VISIBLE);
        imageView_vacant.startAnimation(animation_top);
        textView_by.setVisibility(View.VISIBLE);
        textView_by.startAnimation(animation_top);
        textView_head_vacant.setText(R.string.occupied_time_intro);
        layout_time_vacant.setBackgroundResource(R.drawable.occupied_time);
        layout_time_vacant.startAnimation(animation_bottom);
        layout_time_vacant.setVisibility(View.VISIBLE);
    }

    private void vacant() {
//        Log.e("vacant", "vacant");
        isOccupied = false;
        button_detail.setText(getString(R.string.detail));
        textView_head.setText(meetingRoomname);
        linearLayout_left.setVisibility(View.GONE);
        relativeLayout_vacant.setVisibility(View.VISIBLE);
        imageView_vacant.setBackgroundResource(R.drawable.vacant);
        imageView_vacant.startAnimation(animation_top);
        imageView_vacant.setVisibility(View.VISIBLE);
        textView_by.setVisibility(View.VISIBLE);
        textView_by.startAnimation(animation_top);
        textView_head_vacant.setText(R.string.vacant_time_intro);
        layout_time_vacant.setBackgroundResource(R.drawable.vacant_time);
        layout_time_vacant.startAnimation(animation_bottom);
        layout_time_vacant.setVisibility(View.VISIBLE);
    }

    public Locale getSetLocale() {
        isCN = sharedPreferences.getBoolean("isCn", true);
        if (isCN) {
            //代码将开关画到中文位置
            return Locale.SIMPLIFIED_CHINESE;
        } else {
            //代码将开关画到英文位置
            return Locale.ENGLISH;
        }
    }

    @Override
    public void hydropower() {
        //切换成英文，目前是中文.isCn=true
        //中文滑向英文
//        isCN = sharedPreferences.getBoolean("isCN",true);
        if (isCN) {
//            LogUtils.e("切换成英文按钮","调用了onstop");
            MobclickAgent.onEvent(getApplicationContext(),"changeToEnglish");
            editor.putBoolean("isCn", false);
            editor.commit();
//            changToEnglish();
//            //重启activity
            Intent intent2 = new Intent(this, MainActivity.class);
            intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent2);
            // 杀掉进程
//            onStop();
            onDestroy();
        } else {
            btn_switchbutton.setLeftDis(68);
        }
    }

    @Override
    public void softFloor() {
        //英文滑向中文
//        Log.e("soft","英文滑向中文");
//        isCN = sharedPreferences.getBoolean("isCN",false);
//        Log.e("iscn英文到中文",isCN+"");
        if (isCN) {
            btn_switchbutton.setLeftDis(0);
//            Toast.makeText(getApplicationContext(), "当前语言是中文", Toast.LENGTH_SHORT).show();
        } else {
            MobclickAgent.onEvent(getApplicationContext(),"changeToChinese");
//            LogUtils.e("切换成中文按钮","调用了onstop");
            editor.putBoolean("isCn", true);
            editor.commit();
            //重启activity
            Intent intent1 = new Intent(this, MainActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
//          杀掉进程
//            onStop();
            onDestroy();
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        editor.putBoolean("isDetailOpen", false);
        editor.commit();
        if (keyCode == KeyEvent.KEYCODE_BACK) { // 监控/拦截/屏蔽返回键
//            LogUtils.e("用户点击了KEYCODE_BACK","调用了onstop方法");
//                onStop();
            onDestroy();
        } else if (keyCode == KeyEvent.KEYCODE_MENU) {

        } else if (keyCode == KeyEvent.KEYCODE_HOME) {
                //这里操作是没有返回结果的
//            LogUtils.e("用户点击了KEYCODE_HOME","调用了onstop方法");
//                onStop();
            onDestroy();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        MobclickAgent.onPageStart("MainScreen");
    }

    @Override
    protected void onPause() {
        super.onPause();
        try{
            if (receiver != null){
                unregisterReceiver(receiver);
            }
            FileHelper.delJPGFiles(QRCodeUtil.getFileRoot(getApplicationContext()));
            socket.disconnect();
            socket.off("meetingroomRecord",meetingroomRecord);
            countSetselection=0;
        }catch (Exception e){
            e.printStackTrace();
            MobclickAgent.reportError(getApplicationContext(),"ondestory="+e.getMessage());
        }
        MobclickAgent.onPause(this);
        MobclickAgent.onPageEnd("SplashScreen");
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if(bitmap != null && !bitmap.isRecycled()){
//            bitmap.recycle();
//            bitmap = null;
//        }

//        LogUtils.e("onDestory", "onDestory");
    }


}