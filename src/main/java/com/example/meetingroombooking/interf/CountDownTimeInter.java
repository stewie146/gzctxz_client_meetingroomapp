package com.example.meetingroombooking.interf;

import com.example.meetingroombooking.domain.Meeting;
import com.example.meetingroombooking.domain.MyTime;

import java.util.List;

/**
 * Created by Cathy on 16/12/15.
 */
public interface CountDownTimeInter {
    void occupiedNow();
    void vacantNow();
}
