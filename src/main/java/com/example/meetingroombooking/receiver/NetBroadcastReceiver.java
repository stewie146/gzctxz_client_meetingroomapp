package com.example.meetingroombooking.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;

import com.example.meetingroombooking.MainActivity;
import com.example.meetingroombooking.helper.LogUtils;
import com.example.meetingroombooking.helper.NetUtil;
import com.umeng.analytics.MobclickAgent;


/**
 * Created by Cathy on 17/3/20.
 */

public class NetBroadcastReceiver extends BroadcastReceiver {
    private String TAG="onReceive";
    public NetEvevt evevt;
    public NetBroadcastReceiver() {
    }
    public NetBroadcastReceiver(NetEvevt evevt) {
        this.evevt = evevt;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        // 如果相等的话就说明网络状态发生了变化
        try{
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                // 接口回调传过去状态的类型
                    boolean netWorkState = NetUtil.isNetworkAvailable(context);
                    if (evevt==null){
                        LogUtils.e(TAG,"event==null");
                    }else{
                        LogUtils.e(TAG,netWorkState+"=========");
                        evevt.onNetChange(netWorkState);
                    }
                }
        }catch (Exception e){
            MobclickAgent.reportError(context,"NetBroadcastReceiver="+e.getMessage());
            Log.e(TAG,e.getMessage());
        }
    }
    // 自定义接口
    public interface NetEvevt {
         void onNetChange(boolean netMobile);
    }
}
