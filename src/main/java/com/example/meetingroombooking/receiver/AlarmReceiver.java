package com.example.meetingroombooking.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.meetingroombooking.MainActivity;

/**
 * Created by Cathy on 17/4/11.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("restart")){
            Intent intent1 = new Intent(context, MainActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent1);
            android.os.Process.killProcess(android.os.Process.myPid());
        }else{

        }
    }
    //注意：receiver记得在manifest.xml注册
    }
