package com.example.meetingroombooking;

import com.tencent.tinker.loader.app.TinkerApplication;
import com.tencent.tinker.loader.shareutil.ShareConstants;

/**
 * Created by Cathy on 17/3/17.
 */

public class SampleApplication extends TinkerApplication {

    public SampleApplication() {
        super(ShareConstants.TINKER_ENABLE_ALL, "com.example.meetingroombooking.SampleApplicationLike",
                "com.tencent.tinker.loader.TinkerLoader", false);
    }

}
