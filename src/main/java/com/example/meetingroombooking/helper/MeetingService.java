package com.example.meetingroombooking.helper;

import com.example.meetingroombooking.domain.Meeting;
import com.example.meetingroombooking.domain.MeetingAndRooms;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Cathy on 17/11/30.
 */

public interface MeetingService {
    @GET("current/{id}")
    Call<MeetingAndRooms> getMeeting(@Path("id") String id);
}
