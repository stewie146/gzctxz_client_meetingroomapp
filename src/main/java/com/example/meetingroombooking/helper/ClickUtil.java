package com.example.meetingroombooking.helper;

/**
 * Created by Cathy_panda on 2016/6/14.
 */
public class ClickUtil {
        private static long lastClickTime;
        public static boolean isFastDoubleClick() {
            long time = System.currentTimeMillis();
            long timeD = time - lastClickTime;
            if ( 0 < timeD && timeD < 200) {
                return true;
            }
            lastClickTime = time;
            return false;
        }
    }
