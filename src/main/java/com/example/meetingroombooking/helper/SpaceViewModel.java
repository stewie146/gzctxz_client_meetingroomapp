package com.example.meetingroombooking.helper;

import com.example.meetingroombooking.CommonUrl;
import com.example.meetingroombooking.domain.MeetingAndRooms;
import com.example.meetingroombooking.domain.Space;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Cathy on 17/12/1.
 */

public class SpaceViewModel {
    private Retrofit retrofit;

    public SpaceViewModel(){
        retrofit = new Retrofit.Builder()
                .baseUrl(CommonUrl.SPACEANDROOM)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }
    public Call<Space> getSpaceAndMaeetingRooms(){
        SpaceService spaceService = retrofit.create(SpaceService.class);
        return spaceService.getSpaceAndMeetingRoom();
    }
}
