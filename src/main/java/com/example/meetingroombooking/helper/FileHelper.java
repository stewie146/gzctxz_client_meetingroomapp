package com.example.meetingroombooking.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
/**
 * 文件处理工具类
 * @Date 2016-3-17
 * @Time 下午3:22:01
 */
public class FileHelper
{
    public final static String HEAD_TYPE = "head";

    public final static String HEAD_ROUND_TYPE = "head_round";

    public final static String THUMBNAIL_TYPE = "thumbnail";

    public final static String ORGINAL_TYPE = "orginal";

    public final static String CHAT_TYPE = "chat";

    public final static String ADS_TYPE = "ads";

    public final static String LOCAL_TYPE = "local";

    public final static String CAMERA_TYPE = "camera";

    public final static String CLIP_TYPE = "clip";

    public final static String AUDIO_TYPE = "audio";
    
    public final static String DYNAMIC_PAGE_TYPE = "dynamic_page";

    private static FileHelper instance;

    /**
     * 用户手动保存的图片目录
     */
    private static String mSavePath;

    /**默认的缓存目录*/
    private String cachePath;

    /**是否存在SD卡*/
    public static boolean hasSdcard()
    {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private FileHelper()
    {
    }

    public String getBitmapPath(String type, String name)
    {
        if (type.equals(HEAD_ROUND_TYPE))
        {
            type = HEAD_TYPE;
        }
        String path = cachePath + File.separator + type;
        File file = new File(path);
        if (!file.exists())
        {
            file.mkdirs();
        }
        return path + File.separator + name;
    }

    public String getDirectory(String type)
    {
        if (type.equals(HEAD_ROUND_TYPE))
        {
            type = HEAD_TYPE;
        }
        String path = mSavePath + File.separator + type;
        File file = new File(path);
        if (!file.exists())
        {
            file.mkdirs();
        }
        return path;
    }
    
    /**
     * 获取图片保存路径
     * @return
     */
    public String getImageSaveDirectory(){
    	Log.d(TAG, "mSavePath="+mSavePath);
    	return mSavePath;
    }

    public String getCurrentPhotoPath()
    {
        String parent = getDirectory(CAMERA_TYPE);
        return parent + File.separator + System.currentTimeMillis() + ".jpg";
    }

    public String getClipPhotoPath()
    {
        String parent = getDirectory(CLIP_TYPE);
        return parent + File.separator + System.currentTimeMillis() + ".jpg";
    }

    public String getCurrentRecordPath(String name)
    {
        String parent = getDirectory(AUDIO_TYPE);
        return parent + File.separator + name + ".amr";
    }


    /**递归删除一个目录中的所有文件*/
    public static void delAllFiles(String filepath)
    {
        File f = new File(filepath);
        if (f.exists() && f.isDirectory())
        {

            File delFile[] = f.listFiles();
            if (delFile == null || delFile.length == 0)
            {
                return;
            }
            File[] childFiles = f.listFiles();
            int i = null!=childFiles?childFiles.length:0;
            for (int j = 0; j < i; j++)
            {
                if (delFile[j].isDirectory())
                {
                    delAllFiles(delFile[j].getAbsolutePath());
                }
                delFile[j].delete();
            }
        }
    }
    /***
     * 递归删除文件夹内所有以.jpg结尾的文件
     */

    public static final void delJPGFiles(String filepath){
        File f = new File(filepath);
        if (f.exists() && f.isDirectory())
        {
            File delFile[] = f.listFiles();
            if (delFile == null || delFile.length == 0)
            {
                return;
            }
            File[] childFiles = f.listFiles();
            int i = null!=childFiles?childFiles.length:0;
            for (int j = 0; j < i; j++)
            {
                if (delFile[j].isDirectory())
                {
                    delAllFiles(delFile[j].getAbsolutePath());
                }
                if (delFile[j].getName().endsWith(".jpg")){
                    delFile[j].delete();
                }
            }
        }
    }
    
    private static final String TAG="FileHelper";

    /**把文件读入内存，转变成字节数组的形式*/
    public static byte[] fileToBytes(String filePath)
    {
        byte[] b = null;
        try
        {
            File file = new File(filePath);
            if (file != null)
            {
                FileInputStream fis = new FileInputStream(file);
                if (fis != null)
                {
                    int len = fis.available();
                    b = new byte[len];
                    fis.read(b);
                }
                fis.close();
            }
        }
        catch (Exception e)
        {

        }
        return b;
    }
}
