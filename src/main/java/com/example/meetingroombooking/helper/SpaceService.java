package com.example.meetingroombooking.helper;

import com.example.meetingroombooking.domain.Space;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Cathy on 17/12/1.
 */

public interface SpaceService {
    @GET("getSpaces")
    Call<Space> getSpaceAndMeetingRoom();
}
