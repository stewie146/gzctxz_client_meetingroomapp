package com.example.meetingroombooking.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.util.Log;


/**
 * 时间处理工具类
 * @author Cathy
 * @date 2016-9-30
 * @time 下午2:54:54 http://www.2cto.com/kf/201404/292989.html
 */
public class DateUtils
{
    private static final String TAG="DateUtils";
    
    /**
     * 时间格式
     */
    public static final String DATE_FORMAT_NORMAL = "yyyy-MM-dd HH:mm:ss";
    
    /**
     * 带中文时间格式
     */
    public static final String DATE_FORMAT_CN = "yyyy年MM月dd日";
    
    /**
     * 日期格式（月日）
     */
    public static final String DATE_FORMAT_MMDD = "MM-dd";
    
    /**
     * 时间格式（时分）
     */
    public static final String DATE_FORMAT_HHMM = "HH:mm";
    
    /**
     * 时间格式（年月日）
     */
    public static final String DATE_FORMAT_YYMMDD = "yyyy-MM-dd";
    
    /**
     * 一分钟
     */
    private static final long ONE_MINUTE = 60000L;
    
    /**
     * 一小时
     */
    private static final long ONE_HOUR = 3600000L;
    
    /**
     * 一天
     */
    private static final long ONE_DAY = 86400000L;
    
    /**
     * 一周
     */
    private static final long ONE_WEEK = 604800000L;
    
    private static final String ONE_SECOND_AGO = "秒前";
    
    private static final String ONE_MINUTE_AGO = "分钟前";
    
    private static final String ONE_HOUR_AGO = "小时前";
    
    private static final String ONE_DAY_AGO = "天前";
    
    private static final String ONE_MONTH_AGO = "个月前";
    
    private static final String ONE_YEAR_AGO = "年前";
    
    private static final String HOUR = ":";
    
    private static final String MINUTE = ":";
    
    private static final String SECOND = "";
    
//        @SuppressLint("SimpleDateFormat")
//        public static void main(String[] args) throws ParseException {  
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
//            Date date = format.parse("2015-08-19 12:35:35");  
//            System.out.println(getTime("2015-08-19 14:35:35","HH:mm"));  
//        }  
    
    
    /**
     * 获取今天的日期
     * @param format
     * @return
     */
    public static String getToday(String format){
    	Calendar cal=Calendar.getInstance();
    	//使用格林尼治时域，防止不同时间戳间的时差
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
        cal.setTimeInMillis(System.currentTimeMillis());
        return getDate(cal.getTime(), format);
    }
    
    
    /**
     * 以指定格式从输入时间中获取所需时间
     * @param dateTime 
     * @return
     * @throws ParseException
     */
    public static String getTime(String dateTime,String formatFrom,String formatTo) throws ParseException{
        if(StringUtil.isBlank(dateTime)||StringUtil.isBlank(formatFrom)||StringUtil.isBlank(formatTo)){
            return "";
        }
        SimpleDateFormat dateFormat=new SimpleDateFormat(formatFrom);
        Date date=dateFormat.parse(dateTime);
        dateFormat=new SimpleDateFormat(formatTo);
        return dateFormat.format(date);
    }
    
    /**
     * 计算指定时间与当前时间的差值</p>大于0：指定时间小于当前时间</p>小于0：指定时间大于当前时间
     * @param dateTime 待计算时间
     * @param format 时间格式
     * @return mills
     * @throws ParseException 
     */
    public static long calculateTimeDiff(String dateTime,String format) throws ParseException{
        if(StringUtil.isBlank(dateTime)){
            return 0;
        }
        //时间格式为空时，使用默认时间格式
        if(StringUtil.isBlank(format)){
            format=DATE_FORMAT_NORMAL;
        }
        SimpleDateFormat dateFormat=new SimpleDateFormat(format);
        Date date=dateFormat.parse(dateTime);
        return date.getTime()-System.currentTimeMillis();
    }

    /**
     * 计算直播时间
     * @return
     */
    public static String getLivingTime(String dateTime) throws ParseException {
        long date = System.currentTimeMillis() - Long.valueOf(dateTime);
        return getCountDownTime(date);
    }
    
    /**
     * 计算倒计时
     * @param mills
     * @return
     */
    public static String getCountDownTime(long mills){
        Calendar cal=Calendar.getInstance();
        //使用格林尼治时域，防止不同时间戳间的时差
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
        //or UTC
//        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
        cal.setTimeInMillis(mills);
        int hour=cal.get(Calendar.HOUR_OF_DAY);
        int minute=cal.get(Calendar.MINUTE);
        int second=cal.get(Calendar.SECOND);
        StringBuffer sbTime=new StringBuffer();
//        if(hour>0){
            sbTime.append(formatNumber(hour)).append(HOUR);
            sbTime.append(formatNumber(minute)).append(MINUTE);
            sbTime.append(formatNumber(second)).append(SECOND);
//        }
//        else if(minute>0){
//            sbTime.append(String.valueOf(minute)).append(MINUTE);
//            sbTime.append(String.valueOf(second)).append(SECOND);
//        }else{
//            sbTime.append(String.valueOf(second)).append(SECOND);
//        }
        return sbTime.toString();
    }

    public static String getNowTime(){
        long ct = System.currentTimeMillis();
        String t = String.valueOf(ct);
        return t;
    }
    
    /**
     * 计算倒计时 精确到毫秒
     * @param mills
     * @return
     */
    public static String getCountDownMillTime(long mills){
    	Calendar cal=Calendar.getInstance();
    	//使用格林尼治时域，防止不同时间戳间的时差
    	cal.setTimeZone(TimeZone.getTimeZone("GMT"));
    	//or UTC
//        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
    	cal.setTimeInMillis(mills);
    	int hour=cal.get(Calendar.HOUR_OF_DAY);
    	int minute=cal.get(Calendar.MINUTE);
    	int second=cal.get(Calendar.SECOND);
    	int MI = cal.get(Calendar.MILLISECOND);
    	StringBuffer sbTime=new StringBuffer();
//        if(hour>0){
    	//sbTime.append(formatNumber(hour)).append(HOUR);
    	sbTime.append(formatNumber(minute)).append(MINUTE);
    	sbTime.append(formatNumber(second)).append(":");
    	sbTime.append(formatNumber(MI));
//        }
//        else if(minute>0){
//            sbTime.append(String.valueOf(minute)).append(MINUTE);
//            sbTime.append(String.valueOf(second)).append(SECOND);
//        }else{
//            sbTime.append(String.valueOf(second)).append(SECOND);
//        }
    	return sbTime.toString();
    }
    
    /**
     * 格式化数字为两位显示
     * @param number
     * @return
     */
    public static String formatNumber(int number){
        if(number<=0){
            return "00";
        }
        if(number<10){
            return "0"+String.valueOf(number);
        }
        return String.valueOf(number);
    }
    
    /**
     * 格式化推荐商品的时间显示
     * @return
     * @throws ParseException 
     */
    public static String formatRecommendGoodsTime(String dateTime,String format) throws ParseException{
        if(isToday(dateTime, format)){
            return getTime(dateTime, format,DATE_FORMAT_HHMM);
        }else{
            return getTime(dateTime,format, DATE_FORMAT_MMDD);
        }
    }
    
    /**
     * 判断给定时间是否是今天时间
     * @return
     * @throws ParseException 
     */
    public static boolean isToday(String dateTime,String format) throws ParseException{
        SimpleDateFormat dateFormat=new SimpleDateFormat(format);
        Date date=dateFormat.parse(dateTime);
        if(date.before(todayEnd())&&date.after(todayBegin())){
            return true;
        }
        return false;
    }
    
    /**
     * 判断给定时间是否是昨天时间
     * @return
     * @throws ParseException 
     */
    public static boolean isYesterday(String dateTime,String format) throws ParseException{
    	SimpleDateFormat dateFormat=new SimpleDateFormat(format);
    	Date date=dateFormat.parse(dateTime);
    	if(date.before(todayBegin())&&date.after(yesterdayBegin())){
    		return true;
    	}
    	return false;
    }
    
    /**
     * 判断给定时间是否是前天时间
     * @return
     * @throws ParseException 
     */
    public static boolean isBeforeYesterday(String dateTime,String format) throws ParseException{
    	SimpleDateFormat dateFormat=new SimpleDateFormat(format);
    	Date date=dateFormat.parse(dateTime);
    	if(date.before(yesterdayBegin())&&date.after(beforeYesterdayBegin())){
    		return true;
    	}
    	return false;
    }
    
    /**
     * 获取今天的开始时间
     * @return
     */
    public static Date todayBegin(){
        Calendar cal=Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    
    /**
     * 获取今天的结束时间
     * @return
     */
    public static Date todayEnd(){
        Calendar cal=Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }
    
    /**
     * 获取昨天的开始时间
     * @return
     */
    public static Date yesterdayBegin(){
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(todayBegin());
    	calendar.add(calendar.DATE,-1);//把日期往后增加一天.整数往后推,负数往前移动
        return calendar.getTime();
    }
    
    /**
     * 获取前天的开始时间
     * @return
     */
    public static Date beforeYesterdayBegin(){
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(todayBegin());
    	calendar.add(calendar.DATE,-2);//把日期往后增加一天.整数往后推,负数往前移动
        return calendar.getTime();
    }
    
    
    /**
     * 计算给定时间与当前时间相差 几分钟、几小时等
     * @param date 时间
     * @param format 时间格式
     * @param limitOneDay 是否只计算一天内的时间（true表示大于一天的时间都只显示具体的时间）
     * @return
     * @throws ParseException 
     */
    @SuppressLint("SimpleDateFormat")
    public static String format(String date, String format,boolean limitOneDay)
            throws ParseException
    {
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
        Date newDate = mSimpleDateFormat.parse(date);
        return format(newDate,format,limitOneDay);
        
    }
    
    /**
     * 计算给定时间与当前时间相差 几分钟、几小时等(默认只计算一天内的时间)
     * @param date 时间
     * @param format 时间格式
     * @return
     * @throws ParseException 
     */
    @SuppressLint("SimpleDateFormat")
    public static String format(String date, String format)
            throws ParseException
    {
        if(StringUtil.isBlank(date)||StringUtil.isBlank(format)){
            Log.e(TAG, "参数为空");
            return "";
        }
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
        Date newDate = mSimpleDateFormat.parse(date);
        return format(newDate,format,true);
    }
    
    /**
     * 时间转换：日期转成字符串
     * @param date 时间
     * @param format 时间格式
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static String getDate(Date date, String format, Locale locale){
        if(null==date||StringUtil.isBlank(format)){
            return "";
        }
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format,locale);
        return mSimpleDateFormat.format(date);
    }
    public static String getDate(Date date, String format){
        if(null==date||StringUtil.isBlank(format)){
            return "";
        }
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
        return mSimpleDateFormat.format(date);
    }
    /**
     * 时间转换：日期转成字符串
     * @param date 时间
     * @param format 时间格式
     * @return
     * @throws ParseException 
     */
    @SuppressLint("SimpleDateFormat")
    public static Date getDate(String date,String format) throws ParseException{
    	if(null==date||StringUtil.isBlank(format)){
    		return null;
    	}
    	SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
    	Date newDate = mSimpleDateFormat.parse(date);
    	return newDate;
    }
    /**
     * 计算给定时间与当前时间相差 几分钟、几小时等
     * @param date 时间
     * @param format 时间格式
     * @param limitOneDay 是否只计算一天内的时间（true表示大于一天的时间都只显示具体的时间）
     * @return
     */
    public static String format(Date date,String format,boolean limitOneDay)
    {
        long delta = new Date().getTime() - date.getTime();
        if (delta < 1L * ONE_MINUTE)
        {
            long seconds = toSeconds(delta);
            return (seconds <= 0 ? 1 : seconds) + ONE_SECOND_AGO;
        }
        if (delta < 45L * ONE_MINUTE)
        {
            long minutes = toMinutes(delta);
            return (minutes <= 0 ? 1 : minutes) + ONE_MINUTE_AGO;
        }
        if (delta < 24L * ONE_HOUR)
        {
            long hours = toHours(delta);
            return (hours <= 0 ? 1 : hours) + ONE_HOUR_AGO;
        }
        //添加限制：只计算一天内的时间，超过一天只显示具体时间
        if(limitOneDay){
            return getDate(date, format);
        }
        if (delta < 48L * ONE_HOUR)
        {
            return "昨天";
        }
        if (delta < 30L * ONE_DAY)
        {
            long days = toDays(delta);
            return (days <= 0 ? 1 : days) + ONE_DAY_AGO;
        }
        if (delta < 12L * 4L * ONE_WEEK)
        {
            long months = toMonths(delta);
            return (months <= 0 ? 1 : months) + ONE_MONTH_AGO;
        }
        else
        {
            long years = toYears(delta);
            return (years <= 0 ? 1 : years) + ONE_YEAR_AGO;
        }
    }
    
    /**
     * 计算给定时间与当前时间相差几天
     * @return
     */
    public static String format(String dateTime){
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	Date date = null;
		try {
			date = format.parse(dateTime);
		} catch (ParseException e) {
//			e.printStackTrace();
            Log.e("481DateUtil",e.getMessage());
		}
    	
    	try {
			if (isToday(dateTime, "yyyy-MM-dd HH:mm:ss")) {
				return "今天"+getDate(date, "HH:mm:ss");
			}else if (isYesterday(dateTime, "yyyy-MM-dd HH:mm:ss")) {
				return "昨天"+getDate(date, "HH:mm:ss");
			}else if (isBeforeYesterday(dateTime, "yyyy-MM-dd HH:mm:ss")) {
				return "前天"+getDate(date, "HH:mm:ss");
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	
    	return getDate(date, "yyyy-MM-dd");
    }
    
    private static long toSeconds(long date)
    {
        return date / 1000L;
    }
    
    private static long toMinutes(long date)
    {
        return toSeconds(date) / 60L;
    }
    
    private static long toHours(long date)
    {
        return toMinutes(date) / 60L;
    }
    
    private static long toDays(long date)
    {
        return toHours(date) / 24L;
    }
    
    private static long toMonths(long date)
    {
        return toDays(date) / 30L;
    }
    
    private static long toYears(long date)
    {
        return toMonths(date) / 365L;
    }
    public static String getCurrentDate(){
        Date dates = new Date();
        SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyyMMddHHmmss");
        String dateTime=simpleDateFormat.format(dates);
        return dateTime;
    }
    public static String getCurrentDate(String format){
        Date dates = new Date();
        SimpleDateFormat simpleDateFormat= new SimpleDateFormat(format);
        String stringime=simpleDateFormat.format(dates);
        return stringime;
        }

    public static Date getCurrentDateFormat(String format){
        Date dates = new Date();
        SimpleDateFormat simpleDateFormat= new SimpleDateFormat(format);
        String stringime=simpleDateFormat.format(dates);
        Date dateTime  = null;
        try {
            dateTime = simpleDateFormat.parse(stringime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
            return dateTime;
    }
    public static long toMins(String timeone,String timetwo) throws ParseException {
//        Date timeonedata = new Date(timeone);
        Date timeonedata=getDate(timeone,"yyyyMMddHHmmss");
        Date timetwodata=getDate(timetwo,"yyyyMMddHHmmss");
//        Date timetwodata = new Date(timetwo);
        long l=timeonedata.getTime()-timetwodata.getTime();
        long mins = toMinutes(l);
        return mins;
    }
    public static Map<String,Long> dataFormat(String timeone,String timetwo) throws ParseException {
        Date timeonedata = getDate(timeone,"yyyyMMddHHmmss");
        Date timetwodata = getDate(timetwo,"yyyyMMddHHmmss");

        long l=timeonedata.getTime()-timetwodata.getTime();
        long day=l/(24*60*60*1000);
        long hour=(l/(60*60*1000)-day*24);
        long min=((l/(60*1000))-day*24*60-hour*60);
        long s=(l/1000-day*24*60*60-hour*60*60-min*60);
//        long[] time =new long[]{day,hour,min,s};
        Map<String,Long> time = new HashMap<>();
        time.put("day",day);
        time.put("hour",hour);
        time.put("min",min);
        time.put("s",s);
        return time;
    }
    public static long dataSubReturnLong(String timeone,String timetwo) throws ParseException{
        Date timeonedata = getDate(timeone,"yyyyMMddHHmmss");
        Date timetwodata = getDate(timetwo,"yyyyMMddHHmmss");

        long l=timeonedata.getTime()-timetwodata.getTime();

        return l;

    }


}

