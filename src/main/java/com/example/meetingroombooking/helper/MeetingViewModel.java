package com.example.meetingroombooking.helper;

import com.example.meetingroombooking.CommonUrl;
import com.example.meetingroombooking.domain.MeetingAndRooms;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Cathy on 17/11/30.
 */

public class MeetingViewModel {
    private Retrofit retrofit;

    public MeetingViewModel(){
        retrofit = new Retrofit.Builder()
                .baseUrl(CommonUrl.MEETINGS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }
    public Call<MeetingAndRooms> getMeeting(String id){
        MeetingService meetingService = retrofit.create(MeetingService.class);
        return meetingService.getMeeting(id);
    }
}
