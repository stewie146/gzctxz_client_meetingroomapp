package com.example.meetingroombooking.login;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meetingroombooking.R;
import com.example.meetingroombooking.helper.ActivityCollector;
import com.example.meetingroombooking.helper.ClickUtil;
import com.umeng.analytics.MobclickAgent;

import java.util.Locale;

/**
 * Created by Cathy on 17/6/26.
 */

public abstract class BaseActivity extends Activity implements View.OnClickListener{
    public Typeface typeFaceCenturyGothic;
    public Typeface typeFaceCenturyGothicBi;
    public Typeface typefaceCenturyGothicBold;
    public Typeface typeFaceCenturyGothicItalic;

    public Typeface typefaceDongqing;
    public Typeface typefaceDongqingBold;
    TextView head_text_reminder;
    public BroadcastReceiver receiver;
    IntentFilter filter;
    public ImageView head_image_logo;

     public SharedPreferences sharedPreferences;
     public SharedPreferences.Editor editor;
     public SharedPreferences.Editor editorLogin;

    public ImageView bt_home_logo;

    boolean isCN;
    public Toast toast;
    TextView tv_toast;

    public void initTypeface(){
//        typeFaceCenturyGothic = Typeface.createFromAsset(getAssets(), "CenturyGothic.TTF");
//        typeFaceCenturyGothicBi = Typeface.createFromAsset(getAssets(), "GOTHICBI.TTF");
//        typefaceCenturyGothicBold = Typeface.createFromAsset(getAssets(),"Century Gothic Bold.TTF");
//        typeFaceCenturyGothicItalic = Typeface.createFromAsset(getAssets(),"Century Gothic Italic.ttf");
//        typefaceDongqing = Typeface.createFromAsset(getAssets(), "Chinese.otf");
//        typefaceDongqingBold = Typeface.createFromAsset(getAssets(), "Hiragino-Sans-GB-W6.otf");

    }
    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    public Locale getSetLocale() {
        isCN = sharedPreferences.getBoolean("isCn", true);
        if (isCN) {
            return Locale.SIMPLIFIED_CHINESE;
        } else {
            return Locale.ENGLISH;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCollector.addActivity(this);

        sharedPreferences = getSharedPreferences("iMeeting", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editorLogin = sharedPreferences.edit();
        Resources resources = this.getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = getSetLocale();
        resources.updateConfiguration(configuration, dm);
        toast = new Toast(getApplicationContext());
        View view_toast = LayoutInflater.from(getApplicationContext()).inflate(R.layout.toast_layout,null);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(view_toast);
        tv_toast = (TextView) view_toast.findViewById(R.id.toast);

        initTypeface();


    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    @Override
    protected void onStop() {

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //删除该活动
        ActivityCollector.removeActivity(this);
    }

    public void setHeadReminder(String headReminder){
        head_text_reminder.setText(headReminder);
    }


    /**
     *[初始化控件]
     */
//    public abstract void initViews();
//    /**
//     *[初始化数据]
//     */
//    public abstract void initData();
    /**
     *[初始化参数]
     */
//    public abstract void initParams();

    /**
     * [初始化字体，设置字体]
     */
//    public abstract void setTypeFace();
    /**
     * [设置监听]
     */
//    public abstract void setListener();
    /**
     * [点击]
     */
//    public abstract void widgetClick(View v);

//    @Override
//    public void onClick(View v) {
//        if (ClickUtil.isFastDoubleClick()) {
//
//        } else {
//            widgetClick(v);
//        }
//    }
}

