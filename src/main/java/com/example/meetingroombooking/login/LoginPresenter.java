package com.example.meetingroombooking.login;

/**
 * Created by Cathy on 17/10/10.
 */

public interface LoginPresenter {
    void login(String json);

}
