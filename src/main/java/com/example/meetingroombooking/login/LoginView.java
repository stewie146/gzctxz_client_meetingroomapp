package com.example.meetingroombooking.login;


import com.example.meetingroombooking.domain.SpaceBean;

/**
 * Created by Cathy on 17/10/10.
 */

public interface LoginView {

    void showLoading();

    void hideLoading();

    String getUserName();

    String getUserPassword();

    void toMainActivity(SpaceBean user);

    void showFailedError(String string);

    void showConnectFailedError(Exception e);

}
