package com.example.meetingroombooking.login;


import com.example.meetingroombooking.domain.SpaceBean;

/**
 * Created by Cathy on 17/10/10.
 */

public interface OnLoginListener {
    void loginSuccess(SpaceBean user);

    void loginFailed(String string);

    void loginConnectFailed(Exception e);


}
