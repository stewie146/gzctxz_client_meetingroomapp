package com.example.meetingroombooking.login;

import android.util.Log;

import com.example.meetingroombooking.domain.SpaceBean;
import com.example.meetingroombooking.helper.LogUtils;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by Cathy on 17/10/10.
 */

public class LoginPresenterIml implements LoginPresenter,OnLoginListener {
    //持有model和view的双方引用
    private LoginModel loginModel;
    private LoginView loginView;

    public LoginPresenterIml(LoginView loginView) {
        this.loginView = loginView;
        loginModel = new LoginModelIml();
    }

    @Override
    public void loginSuccess(SpaceBean user) {
        LogUtils.e("spaceIDLoginPresenter",user.getData().getSpace().get_id());
        loginView.toMainActivity(user);
    }

    @Override
    public void loginFailed(String string) {
        loginView.showFailedError(string);
    }

    @Override
    public void loginConnectFailed(Exception e) {
        loginView.showConnectFailedError(e);
    }

    @Override
    public void login(String json) {
//        loginView.showLoading();
        loginModel.login(json,this);

    }
}
