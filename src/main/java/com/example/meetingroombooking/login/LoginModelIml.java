package com.example.meetingroombooking.login;

import android.util.Log;

import com.example.meetingroombooking.CommonUrl;
import com.example.meetingroombooking.domain.SpaceBean;
import com.example.meetingroombooking.helper.LogUtils;
import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Cathy on 17/10/10.
 */

public class LoginModelIml implements LoginModel {
    private OnLoginListener OnLoginListener;
    SpaceBean spaceBean;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    @Override
    public void login(String json, final OnLoginListener onLoginListener) {
        this.OnLoginListener = onLoginListener;
        //提交给服务器
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(10,TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10,TimeUnit.SECONDS)
                .build();
        //上传用户名和密码给服务器
        RequestBody requestBody = RequestBody
                .create(JSON,json);

        final Request request = new Request.Builder()
                .url(CommonUrl.LOGIN)
                .addHeader("content-type", "application/json; charset=utf-8")
                .post(requestBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onLoginListener.loginConnectFailed(e);

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String result = response.body().string();
                    //解析tokenå
                    LogUtils.e("loginresult====",result);
                    Gson gson = new Gson();
                    spaceBean = gson.fromJson(result,SpaceBean.class);
                    if (spaceBean!=null&&spaceBean.getCode()==0){
                        onLoginListener.loginSuccess(spaceBean);
                    }else {
                        onLoginListener.loginFailed(spaceBean.getCode()+spaceBean.getMsg());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
}
