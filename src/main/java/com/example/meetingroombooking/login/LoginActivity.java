package com.example.meetingroombooking.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meetingroombooking.MainActivity;
import com.example.meetingroombooking.R;
import com.example.meetingroombooking.domain.LoginUser;
import com.example.meetingroombooking.domain.SpaceBean;
import com.example.meetingroombooking.helper.ActivityCollector;
import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by Cathy on 17/7/4.
 */

public class LoginActivity extends BaseActivity implements LoginView{
    private EditText edt_name;
    private EditText edt_password;
    private TextView bt_submit;
    private TextView bt_exit;
    ProgressBar progressBar;
    private LoginPresenter loginPresenter;
    private SpaceBean spaceBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initParams();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.activity_login);
        initViews();
        setListener();
        setTypeFace();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            //将所有的activity都出栈
            ActivityCollector.finishAll();
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    public void initViews() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
//        progressBar.setVisibility(View.GONE);
        edt_name = (EditText) findViewById(R.id.textView_login);
        edt_password = (EditText) findViewById(R.id.textView_admin);
        bt_submit = (TextView) findViewById(R.id.textView_login_submit);
        bt_exit = (TextView) findViewById(R.id.textView_login_exit);
    }


    public void initParams() {
        loginPresenter = new LoginPresenterIml(this);
        String token = sharedPreferences.getString("token","");
        String spaceID = sharedPreferences.getString("spaceID","");
        String logo = sharedPreferences.getString("logo","");
        String toLeftLogo = sharedPreferences.getString("topLeftLogo","");
        String slogan = sharedPreferences.getString("slogan","");
        if (TextUtils.isEmpty(token)||TextUtils.isEmpty(spaceID)||TextUtils.isEmpty(logo)||TextUtils.isEmpty(slogan)||TextUtils.isEmpty(toLeftLogo)){
            setContentView(R.layout.activity_login);
            initViews();
            setListener();
            setTypeFace();
        } else {
            spaceBean = new Gson().fromJson(sharedPreferences.getString("spaceBean",""),SpaceBean.class);
            toMainActivity(spaceBean);
        }
    }


    public void setTypeFace() {
        bt_submit.setTypeface(typefaceCenturyGothicBold);
        bt_exit.setTypeface(typefaceCenturyGothicBold);
    }


    public void setListener() {
        bt_submit.setOnClickListener(this);
        bt_exit.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textView_login_submit:

                if (!TextUtils.isEmpty(getUserName())&&!TextUtils.isEmpty(getUserPassword())){
                    LoginUser loginUser = new LoginUser(getUserName(),getUserPassword());
                    String json = new Gson().toJson(loginUser);
                    loginPresenter.login(json);
                }else {
                    Toast.makeText(getApplicationContext(),"请将用户名密码填写完整",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.textView_login_exit:
                clear();
                break;
        }
    }
    public void clear(){
        //退出，清除本地token保存，下一次重新登陆；
        edt_name.setText("");
        edt_password.setText("");
        editorLogin.putString("token","");
        editorLogin.commit();
        editorLogin.putString("spaceID","");
        editorLogin.commit();
        editorLogin.putString("slogan","");
        editorLogin.commit();
        editorLogin.putString("topLeftLogo","");
        editorLogin.commit();
        editorLogin.putString("logo","");
        editorLogin.commit();
        editorLogin.putString("spaceBean","");
        editorLogin.commit();
    }

    @Override
    public void showLoading() {
//        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
//        progressBar.setVisibility(View.GONE);
    }

    @Override
    public String getUserName() {

        return edt_name.getText().toString();

    }

    @Override
    public String getUserPassword() {
        return edt_password.getText().toString();
    }
    String spaceID;
    String logo;
    String slogan;
    String topLeftLogo;
    @Override
    public void toMainActivity(SpaceBean spaceBean) {
        this.spaceBean = spaceBean;
        try{
             editorLogin.putString("spaceBean",new Gson().toJson(spaceBean));
             editorLogin.commit();

            if (spaceBean.getData()!=null) {
                SpaceBean.DataBean spaceBean1 = spaceBean.getData();
                spaceID = !TextUtils.isEmpty(spaceBean1.getSpace().get_id()) ? spaceBean.getData().getSpace().get_id() : "5876e6ee666c49e806f46636";
                logo = !TextUtils.isEmpty(spaceBean1.getConfig().getLogo()) ? spaceBean1.getConfig().getLogo() : "1";
                slogan = !TextUtils.isEmpty(spaceBean1.getConfig().getSlogan()) ? spaceBean1.getConfig().getSlogan() : "1";
                topLeftLogo = !TextUtils.isEmpty(spaceBean1.getConfig().getTopLeftLogo()) ? spaceBean1.getConfig().getTopLeftLogo() : "1";

                editorLogin.putString("token", spaceBean.getToken());
                editorLogin.commit();
                editorLogin.putString("spaceID", spaceID);
                editorLogin.commit();
                editorLogin.putString("logo", logo);
                editorLogin.commit();
                editorLogin.putString("slogan", slogan);
                editorLogin.commit();
                editorLogin.putString("topLeftLogo", topLeftLogo);
                editorLogin.commit();
                //根据spaceid进行网络请求
                Intent mainIntent = new Intent(this, MainActivity.class);
                mainIntent.putExtra("spaceID", spaceID);
                mainIntent.putExtra("logo", logo);
                mainIntent.putExtra("slogan", slogan);
                mainIntent.putExtra("topLeftLogo", topLeftLogo);
                //语言切换
                startActivity(mainIntent);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override   public void showFailedError(final String string) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv_toast.setText("账户名或密码错误");
                MobclickAgent.reportError(getApplicationContext(),"账户名密码输入错误"+string);
                toast.show();
                clear();
            }
        });
    }

    @Override
    public void showConnectFailedError(final Exception e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MobclickAgent.reportError(getApplicationContext(),"loginConnectFailed"+e.getMessage());
                tv_toast.setText("服务器无响应");
                toast.show();
                clear();
            }
        });
    }
}
