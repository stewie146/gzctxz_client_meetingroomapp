package com.example.meetingroombooking.domain;

/**
 * Created by Cathy on 16/9/19.
 */
public class Meeting {
    private String startTime;
    private String endTime;
    private String company;
    private String reason;
    private boolean isNow;
    private boolean isMeeting;


    public boolean isNow() {
        return isNow;
    }

    public void setNow(boolean now) {
        isNow = now;
    }

    public boolean isMeeting() {
        return isMeeting;
    }

    public void setMeeting(boolean meeting) {
        isMeeting = meeting;
    }

    public Meeting() {

    }

    public Meeting(String startTime, String endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Meeting(String startTime, String endTime, String company, String reason) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.company = company;
        this.reason = reason;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "Meeting{" +
                "startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", company='" + company + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
