package com.example.meetingroombooking.domain;

import android.util.Log;

import com.example.meetingroombooking.helper.DateUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class MyTime implements Comparable<MyTime> {
    private long start;
    private long end;
    private boolean isOccupied;
    private String company;
    private List<Meeting> meetingList = new ArrayList<>();

    public List<Meeting> getMeetingList() {
        return meetingList;
    }

    public void setMeetingList(List<Meeting> meetingList) {
        this.meetingList = meetingList;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    //    private long  deadline= DateUtils.getDate();

    public void setStart(long start) {
        this.start = start;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public MyTime() {
    }


    public MyTime(long start, long end) {
        this.start = start;
        this.end = end;
    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public int compareTo(MyTime other) {
        if (start == other.start) {
            return (int) ((long) other.end - (long) end);
        }
        return (int) (start - other.start);
    }

    public MyTime(long start, long end, boolean isOccupied) {
        this.start = start;
        this.end = end;
        this.isOccupied = isOccupied;
    }

    public MyTime(long start, long end, String company) {
        this.start = start;
        this.end = end;
        this.company = company;
    }

    public MyTime(long start, long end, boolean isOccupied, List<Meeting> meetingList) {
        this.start = start;
        this.end = end;
        this.isOccupied = isOccupied;
        this.meetingList = meetingList;
    }

    @Override
    public String toString() {
        return "MyTime{" +
                "start=" + start +
                ", end=" + end +
                ", isOccupied=" + isOccupied +
                ", company='" + company + '\'' +
                '}';
    }

    //计算会议室时间间隔，时间合并算法，
    public static List<MyTime> CountTime(List<MyTime> myTimeList, long s1, long e1, List<Meeting> meetings) throws Exception {
        List<MyTime> myTimes = new ArrayList<>();
        Collections.sort(myTimeList);
//        System.out.println(myTimeList.size());
        Stack s = new Stack();
        Stack e = new Stack();
        List<Boolean> b = new ArrayList<>();
        List<String> c = new ArrayList<>();
        s.push(s1);
        e.push(e1);
        //计算首个是什么。
//        b.add(true);
        int count = 0;
        for (MyTime time : myTimeList) {
//            System.out.println(time.getStart() + " " + time.getEnd());
            if (time.getStart() > time.getEnd())
                throw new Exception("The time is incorrect.");

            if (time.getStart() > (long) e.peek()) //没有交集
            {
                if (count == 0) {
                    //if (meetings.){
                        b.add(false);
                    //}else{
                        //b.add(true);
                    //}
                    c.add(" ");
                }
                count++;
                s.push(e.peek());//将没有活动的时间段也算出来了;
                e.push(time.getStart());//将没有活动的时间段也算出来了;
//                b.remove(b.size()-1);
                b.add(false);//false
                c.add("");
                s.push(time.getStart());
                e.push(time.getEnd());
                b.add(true);//true
                c.add(time.getCompany());
            } else if (time.getEnd() > (long) e.peek()) //有部分交集，取并集
            {
                if (count == 0) {
                    b.add(true);
                    c.add(time.getCompany());
                }
                count++;
                e.pop();
                e.push(time.getEnd());
            }
        }
        long total = 0;
        int i = b.size() - 1;
        if (s.size() - b.size() == 1) {
            s.pop();
            e.pop();
        } else if (s.size() - b.size() == 1) {
            s.pop();
            e.pop();
            s.pop();
            e.pop();
        }
//            Log.e("bsize", b.size() + "..." + s.size());
        while (!s.empty()) {
//            System.out.println(s.peek() + " ~ " + e.peek() + " ~ "
//                    + b.get(i) + "~" + c.get(i)
//            );
            MyTime myTimenow = new MyTime((long) s.peek(), (long) e.peek(), b.get(i));
            myTimenow.setCompany(c.get(i));
            myTimes.add(0, myTimenow);
            i--;
            total += (long) e.pop() - (long) s.pop();
        }
        //加上最后一个会议到当天时间结束之间的状态。
            long deadline = Long.parseLong(meetings.get(meetings.size() - 1).getEndTime());
            if (myTimes.size() == 0) {
                if (s1 < deadline) {
                    myTimes.add(new MyTime(s1, deadline, false, null));
                }
            } else {
                if (myTimes.get(myTimes.size() - 1).getEnd() < deadline) {
                    myTimes.add(new MyTime(myTimes.get(myTimes.size() - 1).getEnd(), deadline, false, null));
                }
            }
        if (myTimes.size() > 1) {
            if (myTimes.get(0).isOccupied() == false) {

                //else{
                myTimes.remove(0);
                int a = (s1+"").compareTo(meetings.get(0).getStartTime());
                Log.e("s----","s=="+s1+"startTime"+meetings.get(0).getStartTime());
                if (a<0){
                    myTimes.get(0).setOccupied(true);
                }
                //}
//                myTimes.remove(0);
            }
        }
//        Log.e("最后的时间", myTimes.toString());
        return myTimes;
    }

    public static List<MyTime> CountTimeNoMeeting(List<MyTime> myTimeList, long s1, long e1) throws Exception {
        List<MyTime> myTimes = new ArrayList<>();
        Collections.sort(myTimeList);
//        System.out.println(myTimeList.size());
        Stack s = new Stack();
        Stack e = new Stack();
        List<Boolean> b = new ArrayList<>();
        List<String> c = new ArrayList<>();
        s.push(s1);
        e.push(e1);
        int count = 0;
        for (MyTime time : myTimeList) {
//            System.out.println(time.getStart() + " " + time.getEnd());
            if (time.getStart() > time.getEnd())
                throw new Exception("The time is incorrect.");

            if (time.getStart() > (long) e.peek()) //没有交集
            {
                if (count == 0) {
                    b.add(false);
                    c.add("");
                }
                count++;
                s.push(e.peek());//将没有活动的时间段也算出来了;
                e.push(time.getStart());//将没有活动的时间段也算出来了;
//                b.remove(b.size()-1);
                b.add(true);//false
                c.add("会议室暂未开放预订哦！");
                s.push(time.getStart());
                e.push(time.getEnd());
                b.add(false);//true
                c.add("当天无安排哦，赶快预定！");
            } else if (time.getEnd() > (long) e.peek()) //有部分交集，取并集
            {
                if (count == 0) {
                    b.add(false);
                    c.add("当天无安排哦，赶快预定！");
                }
                count++;
                e.pop();
                e.push(time.getEnd());
            }
        }
        long total = 0;
        int i = b.size() - 1;
        if (s.size() - b.size() == 1) {
            s.pop();
            e.pop();
        } else if (s.size() - b.size() == 1) {
            s.pop();
            e.pop();
            s.pop();
            e.pop();
        }
        while (!s.empty()) {
//            System.out.println(s.peek() + " ~ " + e.peek() + " ~ "
//                    + b.get(i) + "~" + c.get(i)
//            );
            MyTime myTimenow = new MyTime((long) s.peek(), (long) e.peek(), b.get(i));
            myTimenow.setCompany(c.get(i));
            myTimes.add(0, myTimenow);
            i--;
            total += (long) e.pop() - (long) s.pop();
        }
        if (myTimes.size() > 1) {
            if (myTimes.get(0).isOccupied() == false) {

                myTimes.remove(0);
            }
        }
        Log.e("没有会议的最后的时间", myTimes.toString());
        return myTimes;
    }

    public static List<MyTime> CountTimeForUsers(List<MyTime> myTimeList,long s1,long e1,List<Meeting> meetings) throws Exception {
        List<MyTime> myTimesforUser = new ArrayList<>();
        Collections.sort(myTimeList);
        Stack s = new Stack();
        Stack e = new Stack();
        List<Boolean> b =new ArrayList<>();
        List<String> c = new ArrayList<>();
        s.push(s1);
        e.push(e1);

        int count =0;
        for (MyTime time : myTimeList)
        {
//            System.out.println(time.getStart() + " = " + time.getEnd());
            if (time.getStart() > time.getEnd())
                throw new Exception("The time is incorrect.");
            if (time.getStart() > (long)e.peek()) //没有交集
            {
                if (count==0){
                    b.add(false);
                    c.add("");
                }
                count++;
                s.push(time.getStart());
                e.push(time.getEnd());
                b.add(true);//true
                c.add(time.getCompany());
            }else if (time.getStart() == (long)e.peek()){
                if (count==0){
                    b.add(false);
                    c.add("");
                }
                count++;
                s.push(time.getStart());
                e.push(time.getEnd());
                b.add(true);//true
                c.add(time.getCompany());
            }
            else if (time.getEnd() > (long)e.peek()) //有部分交集，取并集
            {
                if (count==0){
                    b.add(true);
                    c.add(time.getCompany());
                }
                count++;
                e.pop();
                e.push(time.getEnd());
            }
        }
        long total = 0;
        int i =b.size()-1;
//        Log.e("c.......",c.toString());
        if (s.size()-b.size()==1){
            s.pop();
            e.pop();
        }else if (s.size()-b.size()==1){
            s.pop();
            e.pop();
            s.pop();
            e.pop();
        }
//        Log.e("bsize",b.size()+".++."+s.size());
        while (!s.empty())
        {
//            System.out.println(s.peek() + " = " + e.peek()+" = "
//                    +b.get(i)+"="+c.get(i)
//            );

            MyTime myTimenow = new MyTime((long)s.peek(),(long)e.peek(),b.get(i));
            myTimenow.setCompany(c.get(i));
            myTimesforUser.add(0,myTimenow);
            i--;
            total += (long)e.pop() - (long)s.pop();
        }
//        System.out.println(myTimesforUser.toString());
        if (myTimesforUser.size()>1){
            if (myTimesforUser.get(0).isOccupied()==false){
                    myTimesforUser.remove(0);
//                }
                int a = (s1+"").compareTo(meetings.get(0).getStartTime());
//                Log.e("s----","s=="+s1+"startTime"+meetings.get(0).getStartTime());
                if (a<0){
                    myTimesforUser.get(0).setOccupied(true);
                    MyTime myTime = new MyTime(s1,Long.parseLong(meetings.get(0).getStartTime()),true);
                    myTime.setCompany("");
                    myTimesforUser.add(0,myTime);
                }
            }
        }
        return  myTimesforUser;
    }
}



