package com.example.meetingroombooking.domain;

import java.util.Arrays;

/**
 * Created by Cathy on 17/10/11.
 */

public class SpaceBean {
    private int code;
    private String msg;
    private DataBean data;

    public SpaceBean() {
    }

    public SpaceBean(int code, String msg, DataBean data, String token) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.token = token;
    }

    @Override
    public String toString() {
        return "SpaceBean{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                ", token='" + token + '\'' +
                '}';
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public SpaceBean.DataBean getData() {
        return data;
    }

    public void setData(SpaceBean.DataBean data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static class DataBean{
        private String id;
        private Space space;
        private Config config;

        public Config getConfig() {
            return config;
        }

        public void setConfig(Config config) {
            this.config = config;
        }

        public DataBean() {
        }

        public DataBean(String id, Space space) {
            this.id = id;
            this.space = space;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "id='" + id + '\'' +
                    ", space=" + space +
                    ", config=" + config +
                    '}';
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Space getSpace() {
            return space;
        }

        public void setSpace(Space space) {
            this.space = space;
        }

        public static class Space{
            private String _id;
            private String[] meetingRooms;

            @Override
            public String toString() {
                return "Space{" +
                        "_id='" + _id + '\'' +
                        ", meetingRooms=" + Arrays.toString(meetingRooms) +
                        '}';
            }

            public Space() {
            }

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String[] getMeetingRooms() {
                return meetingRooms;
            }

            public void setMeetingRooms(String[] meetingRooms) {
                this.meetingRooms = meetingRooms;
            }
        }
        public static class Config{
            private String _id;
            /**
             * 除去首页，其他页面左上角的商标名称
             */
            private String topLeftLogo;
            /**
             * 首页的大商标图片 i-checkin
             */
            private String slogan;
            /**
             * 首页左上角的logo，同时也是其他页面的底部中央button的图片。
             */
            private String logo;

            @Override
            public String toString() {
                return "Config{" +
                        "_id='" + _id + '\'' +
                        ", topLeftLogo='" + topLeftLogo + '\'' +
                        ", slogan='" + slogan + '\'' +
                        ", logo='" + logo + '\'' +
                        '}';
            }

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getTopLeftLogo() {
                return topLeftLogo;
            }

            public void setTopLeftLogo(String topLeftLogo) {
                this.topLeftLogo = topLeftLogo;
            }

            public String getSlogan() {
                return slogan;
            }

            public void setSlogan(String slogan) {
                this.slogan = slogan;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }

    }
    private String token;

}
