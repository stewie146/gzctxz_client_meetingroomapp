package com.example.meetingroombooking.domain;

import java.util.List;

/**
 * Created by Cathy on 16/12/26.
 */
public class MeetingAndRooms {
    private String msg;
    private List<DataBean> data;
    private int code;

    public MeetingAndRooms() {
    }

    public MeetingAndRooms(String msg, List<DataBean> data, int code) {
        this.msg = msg;
        this.data = data;
        this.code = code;
    }

    @Override
    public String toString() {
        return "MeetingAndRooms{" +
                "msg='" + msg + '\'' +
                ", data=" + data +
                ", code=" + code +
                '}';
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public static class DataBean {
        private String _id;
        private String name;
        private String openTime;
        private String closeTime;
        private List<RecordsBean> records;

        public DataBean() {
        }


        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOpenTime() {
            return openTime;
        }

        public void setOpenTime(String openTime) {
            this.openTime = openTime;
        }

        public String getCloseTime() {
            return closeTime;
        }

        public void setCloseTime(String closeTime) {
            this.closeTime = closeTime;
        }

        public DataBean(String _id, String name, String openTime, String closeTime, List<RecordsBean> records) {
            this._id = _id;
            this.name = name;
            this.openTime = openTime;
            this.closeTime = closeTime;
            this.records = records;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "_id='" + _id + '\'' +
                    ", name='" + name + '\'' +
                    ", openTime='" + openTime + '\'' +
                    ", closeTime='" + closeTime + '\'' +
                    ", records=" + records +
                    '}';
        }

        public static class RecordsBean {
            private String _id;
            private String title;
            private String startTime;
            private String endTime;
            private boolean status;

            @Override
            public String toString() {
                return "MeetingBean{" +
                        "_id='" + _id + '\'' +
                        ", title='" + title + '\'' +
                        ", startTime='" + startTime + '\'' +
                        ", endTime='" + endTime + '\'' +
                        ", status=" + status +
                        '}';
            }

            public boolean isStatus() {
                return status;
            }

            public RecordsBean() {
            }

            public RecordsBean(String _id, String title, String startTime, String endTime, boolean status) {
                this._id = _id;
                this.title = title;
                this.startTime = startTime;
                this.endTime = endTime;
                this.status = status;
            }

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getStartTime() {
                return startTime;
            }

            public void setStartTime(String startTime) {
                this.startTime = startTime;
            }

            public String getEndTime() {
                return endTime;
            }

            public void setEndTime(String endTime) {
                this.endTime = endTime;
            }

            public boolean getStatus() {
                return status;
            }

            public void setStatus(boolean status) {
                this.status = status;
            }

        }
    }
}
