package com.example.meetingroombooking.domain;

/**
 * Created by Cathy on 17/10/10.
 */

public class LoginUser {
    private String name;
    private String password;

    public LoginUser() {
    }

    public LoginUser(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginUser{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
