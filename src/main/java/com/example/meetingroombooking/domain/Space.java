package com.example.meetingroombooking.domain;

import java.util.List;

/**
 * Created by Cathy on 16/12/22.
 */
public class Space {

    private String msg;
    private List<DataBean> data;
    private int code;

    public Space() {
    }

    public Space(String msg, int code, List<DataBean> data) {
        this.msg = msg;
        this.code = code;
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Space{" +
                "msg='" + msg + '\'' +
                ", data=" + data +
                ", code=" + code +
                '}';
    }

    public static class DataBean{
        private String _id;
        private String name;
        private List<RoomsBean> rooms;

        public DataBean(String _id, String name, List<RoomsBean> rooms) {
            this._id = _id;
            this.name = name;
            this.rooms = rooms;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public DataBean() {
        }


        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public List<RoomsBean> getRooms() {
            return rooms;
        }

        public void setRooms(List<RoomsBean> rooms) {
            this.rooms = rooms;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "_id='" + _id + '\'' +
                    ", name='" + name + '\'' +
                    ", rooms=" + rooms +
                    '}';
        }


        public static class RoomsBean{
            private String _id;
            private String name;

            public RoomsBean() {
            }

            public RoomsBean(String _id, String name) {
                this._id = _id;
                this.name = name;
            }

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return "RoomsBean{" +
                        "_id='" + _id + '\'' +
                        ", name='" + name + '\'' +
                        '}';
            }
        }
    }
}
