package com.example.meetingroombooking.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meetingroombooking.R;
import com.example.meetingroombooking.domain.Meeting;
import com.example.meetingroombooking.helper.DateUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cathy on 16/9/19.
 */
public class MeetingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private List<Meeting> meetings;
    private Context context;
    private LayoutInflater layoutInflater;
    private Typeface typeFaceavermedium;
    private Typeface typeFaceBlack;
    private Typeface typefaceGC;
    public MeetingAdapter(Context context) {
        this.context=context;
        meetings=new ArrayList<Meeting>();
        layoutInflater=LayoutInflater.from(context);
        typeFaceavermedium = Typeface.createFromAsset(context.getAssets(), "Avenir-Medium.otf");
        typeFaceBlack = Typeface.createFromAsset(context.getAssets(), "Avenir-Black.ttf");
        typefaceGC = Typeface.createFromAsset(context.getAssets(), "Avenir Next LT W01 Ultra Light.ttf");
    }
    public void setData(List<Meeting> meetings){
        this.meetings=meetings;
//        notifyDataSetChanged();
    }

    //判断条目类型
    String startTime;
    String endTime;
    int b ;
    int a;
    Meeting meeting ;
    @Override
    public int getItemViewType(int position) {
        String currentTime= DateUtils.getCurrentDate();
        if (meetings.size()==0){
//            //return 3;
//            Log.e("记录为空","记录为空");
        }else{
         meeting = meetings.get(position);
        //处理数字格式"20161008150000"
         startTime= meeting.getStartTime();
//                 .replace("/","").replace(":","").replace(" ","");
         endTime= meeting.getEndTime();
//                 .replace("/","").replace(":","").replace(" ","");
         b = startTime.compareTo(currentTime);//startTime-currentTime
         a = endTime.compareTo(currentTime);//endTime-currentTime
        if ((b<=0)&&(a>0)){
            return 2;
        }else
            if (a<=0){
                return 1;
            }else if(b>=1) {
                return 2;
                }
            }
        return 2;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view ;
        RecyclerView.ViewHolder holder = null;
        view = layoutInflater.inflate(R.layout.room_meeting_time_item_white, parent, false);
        holder = new MeetingUpComingViewHolder(view);
        switch (viewType){
            case 1:
                view = layoutInflater.inflate(R.layout.room_meeting_time_item_gray, parent, false);
                holder = new MeetingPastViewHolder(view);
                break;
            case 2:
                view = layoutInflater.inflate(R.layout.room_meeting_time_item_white, parent, false);
                holder = new MeetingUpComingViewHolder(view);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (meetings.size()==0){
        }else{
        Meeting meeting = meetings.get(position);
        switch (getItemViewType(position)){
            case 1:
                MeetingPastViewHolder meetingPastViewHolder = (MeetingPastViewHolder) holder;
                String startTimepast=meeting.getStartTime().substring(8,10)+":"+meeting.getStartTime().substring(10,12);
                meetingPastViewHolder.textView_startTime.setTypeface(typeFaceBlack);
                meetingPastViewHolder.textView_startTime.setText(startTimepast);
                break;
            case 2:
                MeetingUpComingViewHolder meetingUpComingViewHolder= (MeetingUpComingViewHolder) holder;
                String startTimeupcoming=meeting.getStartTime().substring(8,10)+":"+meeting.getStartTime().substring(10,12);
                meetingUpComingViewHolder.textView_startTime.setTypeface(typeFaceBlack);
                meetingUpComingViewHolder.textView_startTime.setText(startTimeupcoming);
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return meetings.size()>0?meetings.size():1;
    }
}
class MeetingPastViewHolder extends RecyclerView.ViewHolder{
    TextView textView_startTime;
    public MeetingPastViewHolder(View itemView) {
        super(itemView);
        textView_startTime= (TextView) itemView.findViewById(R.id.time_on);
    }
}
class MeetingUpComingViewHolder extends RecyclerView.ViewHolder{
    TextView textView_startTime;
    public MeetingUpComingViewHolder(View itemView) {
        super(itemView);
        textView_startTime= (TextView) itemView.findViewById(R.id.time_on);
    }
}